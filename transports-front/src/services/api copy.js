import axios from 'axios'
 

const apiClient = axios.create({
    baseURL: 'http://localhost:8000/',
    withCredentials: true,
    
});

apiClient.interceptors.response.use(response => response, error => {
    if (error.response.status === 401) {
        //logOut()
        console.log('non autorisé')
        return Promise.reject()
    }

    return Promise.reject(error)
})

 
export default apiClient;