import React from 'react';
import {connect} from 'react-redux';
import { apiDatasCall } from '../../store/datas/actionData'
import {Row, Col, Table, Tabs, Button, Tab} from 'react-bootstrap';
import * as actionTypes from "../../store/actions";
import Aux from "../../hoc/_Aux";
import Card from "../../App/components/MainCard";
import {Image} from 'antd';
import logo from '../../assets/images/logoTransports.png'
import UcFirst from "../../App/components/UcFirst"
import DEMO from "../../store/constant";
import Loader from '../../App/layout/Loader';
import { Redirect } from 'react-router-dom';
import SignIn1 from '../Authentication/SignIn/SignIn1'
import avatar1 from '../../assets/images/user/avatar-1.jpg';
import avatar2 from '../../assets/images/user/avatar-2.jpg';
import avatar3 from '../../assets/images/user/avatar-3.jpg';
import apiClient from '../../services/api';



class Dashboard extends React.Component {

  componentDidMount(){
    if(this.props.loggedIn){
      this.props.apiDatasDispatchZones()
      this.props.apiDatasDispatchEntreprises()
      this.props.apiDatasDispatchContrats()
      this.props.apiDatasDispatchMarchandises()
      this.props.apiDatasDispatchNSTs()
      this.props.apiDatasDispatchPays()
      this.props.apiDatasDispatchPorts()
      this.props.apiDatasDispatchUsers()
      this.props.apiDatasDispatchReleves()
      this.props.apiDatasDispatchRoles()
    }
    else{
      this.render(
        <Loader/>
      )
      
    }
              
  }

  logout(){
       
     window.location.href = '/'
  }


  render() {
      
  
      
      const {role, name} = this.props
      //console.log('props du dashboard ' + role)
        return (
          
          <Aux>
                            
                <Row>
                    <Col>
                        <Card title='Bienvenue sur ...' isOption>
                        <img style={{width: 200, margin: 20}} src={logo} alt="logo"/>
                            <p>
                              Vous êtes connecté(e) en tant que <span className="text-primary mb-1">{name}</span> et vous êtes titulaire du rôle <span className="text-primary mb-1">{role}</span>
                            </p>
                            <Button onClick={()=>this.logout()} variant={'outline-primary'}><UcFirst text='logout' /></Button>  
                        </Card>
                    </Col>
                    

                </Row>
              </Aux> 
            
        );
    }
}

const mapStateToProps = (state) =>{
  return {
      role: state.bases.role,
      loggedIn: state.bases.loggedIn,
      name: state.bases.userName,
      isLoading: state.datas.isLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
      apiDatasDispatchZones: () => dispatch(apiDatasCall('zones')),
      apiDatasDispatchEntreprises: () => dispatch(apiDatasCall('entreprises')),
      apiDatasDispatchContrats: () => dispatch(apiDatasCall('contrats')),
      apiDatasDispatchMarchandises: () => dispatch(apiDatasCall('marchandises')),
      apiDatasDispatchNSTs: () => dispatch(apiDatasCall('nsts')),
      apiDatasDispatchPays: () => dispatch(apiDatasCall('pays')),
      apiDatasDispatchPorts: () => dispatch(apiDatasCall('ports')),
      apiDatasDispatchUsers: () => dispatch(apiDatasCall('users')),
      apiDatasDispatchReleves: () => dispatch(apiDatasCall('releves')),
      apiDatasDispatchRoles: () => dispatch(apiDatasCall('roles')),
      setLogout: () => dispatch({type: actionTypes.LOGOUT})
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);