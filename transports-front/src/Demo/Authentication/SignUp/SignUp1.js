import React from 'react';
import {NavLink, Redirect } from 'react-router-dom';

import './../../../assets/scss/style.scss';
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";

import { connect } from 'react-redux'
import * as actionTypes from '../../../store/actions'
import apiClient from '../../../services/api'
import Cookies from 'js-cookie'
import Dashboard from '../../../Demo/Dashboard/Default'

import SignIn1 from '../SignIn/SignIn1'


const SignUp1 = ({role, applyRole, setLogin, setUserName}) => {


    const [name, setName] = React.useState('')
    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [confirm_password, setConfirmPassword] = React.useState('')
    const [message, setMessage] = React.useState('')

    const handleSubmit = (e) => {
        e.preventDefault()
        apiClient.get('sanctum/csrf-cookie')
            .then(response => {
                apiClient.post('api/register',{
                    name: name,
                    email: email,
                    password: password,
                    confirm_password: confirm_password
                }).then(response => {
                    if(response.data.error){
                        console.log(response.data.error)
                    }
                    else{
                        //console.log(response.data)
                        setMessage('login')                
                    }
            
            }).catch(error => {
                console.log(error)
                setMessage('Les deux mots de passe doivent être identiques')
        })
        });
    }
    
    if(message === 'login'){
        return(<Redirect to="/auth/signin-1" component={SignIn1}/>)
    } 
    else {
        return(
            <Aux>
                <Breadcrumb/>
                <div className="auth-wrapper">
                    <div className="auth-content">
                        <div className="auth-bg">
                            <span className="r"/>
                            <span className="r s"/>
                            <span className="r s"/>
                            <span className="r"/>
                        </div>
                        <div className="card">
                            <div className="card-body text-center">
                                <div className="mb-4">
                                    <i className="feather icon-user-plus auth-icon"/>
                                </div>
                                <h3 className="mb-4">Nouvel enregistrement</h3>
                                <form onSubmit = {handleSubmit}>
                                    <div className="input-group mb-3">
                                        <input 
                                            type="text" 
                                            className="form-control" 
                                            placeholder="username" 
                                            name="name" 
                                            value={name}
                                            onChange={e => setName(e.target.value)}
                                        />
                                    </div>
                                    <div className="input-group mb-3">
                                        <input 
                                            type="email" 
                                            className="form-control" 
                                            placeholder="email"
                                            name="email"
                                            value={email}
                                            onChange={e => setEmail(e.target.value)}
                                        />
                                    </div>
                                    <div className="input-group mb-4">
                                        <input 
                                            type="password" 
                                            className="form-control" 
                                            placeholder="password"
                                            name="password"
                                            value={password}
                                            onChange={e => setPassword(e.target.value)}
                                        />
                                    </div>
                                    <div className="input-group mb-4">
                                        <input 
                                            type="password" 
                                            className="form-control" 
                                            placeholder="confirm password"
                                            name="confirm_password"
                                            value={confirm_password}
                                            onChange={e => setConfirmPassword(e.target.value)}
                                        />
                                    </div>
                                    
                                    <button className="btn btn-primary shadow-2 mb-4">Enregistrez-vous</button>
                                </form>
                                {message ? <div class="alert alert-warning" role="alert">{message}</div> : ''}
                                <p className="mb-0 text-muted">Vous avez déjà un compte ? <NavLink to="/auth/signin-1">Login</NavLink></p>
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
    );
        };
    
}

const mapStateToProps = state => {
    return {
        role: state.bases.roles,
        loggedIn: state.bases.loggedIn,
        userName: state.bases.userName
    }
}

const mapDispatchToProps = dispatch => {
    return {
        applyRole: (role) => dispatch({type: actionTypes.APPLY_ROLE, role}),
        setLogin: (loggedIn) => dispatch({type: actionTypes.LOGIN, loggedIn}),
        setUserName: (userName) => dispatch({type: actionTypes.SET_USERNAME, userName})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp1);