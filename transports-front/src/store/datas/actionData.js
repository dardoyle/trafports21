import { LOAD_DATAS, LOAD_DATAS_SUCCESS, LOAD_DATAS_ERROR  } from "./type";
import apiClient from '../../services/api'

/**
 * 
 * @param {*} datas en provenance de l'api 
 * @returns datas du state
 */
export const loadDatas = () => {
    return{
        type: LOAD_DATAS
    }
}

export const loadDatasSuccess = (datas) => {
    return{
        type: LOAD_DATAS_SUCCESS,
        payload: datas.datas,
        table: datas.table
    }
}

export const loadDatasError = (error) => {
    return{
        type: LOAD_DATAS_ERROR,
        payload: error
    }
}

// retourne une fonction qui peut faire des side effects, un appel AXIOS à l'api en l'occurence
export const apiDatasCall = (table) => {
    return(dispatch) => {
        
        dispatch(loadDatas())
        
        apiClient.get(`/api/${table}`)
                    
            .then(response => {
                dispatch(loadDatasSuccess({datas : response.data, table}))
            })
            .catch(error => {
                dispatch(loadDatasError(error))
            })
    }
}