import { LOAD_DATAS, LOAD_DATAS_SUCCESS, LOAD_DATAS_ERROR  } from "./type"

const initialStateDatas = {
    isLoading: false,
    datas: [],
    error: ''
};

// fonction pure qui retourne un nouveau state
const dataReducer = (state = initialStateDatas, action) => {
    switch(action.type){
        case LOAD_DATAS:
            return{
                ...state,
                isLoading: true
            }
        case LOAD_DATAS_SUCCESS:
            return{
                ...state,
                isLoading: false,
                [action.table]: action.payload,
                error: ''
            }
        case LOAD_DATAS_ERROR:
            return{
                ...state,
                isLoading: false,
                datas: [],
                error: action.payload
        }
            
        default:
            return state;
    }
}

export default dataReducer