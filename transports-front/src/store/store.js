import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import dataReducer from './datas/reducerData'
import contratReducer from './contrats/reducerContrat'
import reducer from './reducer'
import thunk from 'redux-thunk'

const rootReducer = combineReducers({
    bases: reducer,
    datas: dataReducer,
    contrats: contratReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));



//const store = createStore(rootReducer)

export default store