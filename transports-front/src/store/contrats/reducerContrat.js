import * as actionTypes from "./actionContrat"; 

const initialStateContrats = {
    contrats: []
};

const contratReducer = (state = initialStateContrats, action) => {
    switch(action.type){
        case actionTypes.retrieveContratsSuccess:
            return{
                ...state,
                contrats: action.contrats
            }
            
        default:
            return state;
    }
}

export default contratReducer