import { RETRIEVE_CONTRATS_SUCCESS } from "./type";


/**
 * 
 * @param {*} contrats en provenance de l'api 
 * @returns contrats du state
 */
export const retrieveContratsSuccess = (contrats) => {
    return{
        type: RETRIEVE_CONTRATS_SUCCESS, 
        payload: contrats
    }
}