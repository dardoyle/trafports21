import apiClient from '../services/api'
import { retrieveZonesSuccess } from './actions'

export default () => {
    return dispatch => {
        apiClient.get('/api/zones')
        .then(response => {
            response.data.map(zone => ({
                id:  zone.id,
                nom: zone.nom,
                created_at: zone.created_at,
                updated_at: zone.updated_at
            }))
        })
        .then(zones => dispatch(retrieveZonesSuccess(zones)))
            //.then(datas => dispatch response.data)
            .catch(error => console.log(error))
        }
    }
    
        
