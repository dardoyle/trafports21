import React, {Component} from 'react';
import {Row,
        Col,
        Button,
        Card,
        Table,
        OverlayTrigger,
        Tooltip,
        ButtonToolbar,
        Dropdown,
        DropdownButton,
        SplitButton } from 'react-bootstrap';
import UcFirst from "../../App/components/UcFirst"    

import Aux from "../../hoc/_Aux";
//import Card from "../../App/components/MainCard";

import apiClient from '../../services/api';
import { connect } from 'react-redux'

const dataPerPage = 10
let arrayToHoldData = []

const GetData = (props) => {
    
    const table = (props.table)
    //console.log(table)
    const [datas, setDatas] = React.useState([])
    const [listToShow, setListToShow] = React.useState([])
    const [next, setNext] = React.useState(10)

    const loopWithSlice = (start, end) => {
        const slicedData = datas.slice(start, end)
        arrayToHoldData = [...arrayToHoldData, ...slicedData]
        setListToShow(arrayToHoldData)
    }
       
    React.useEffect(() => {
        if (props.loggedIn){
            apiClient.get('/api/' + table)
            .then(response => {
                setDatas(response.data)
            })
            .catch(error => console.log(error))
            }
        }, [])

    React.useEffect(() => {
        loopWithSlice(0, dataPerPage)
        }, [])


    

    const handleShowMore = () =>{
        loopWithSlice(next, next + dataPerPage)
        setNext(next + dataPerPage)
    }

    

    let tableau = ''
    let datasTitres = []
    let firstData = datas[0]

    if(firstData != undefined){
        datasTitres = Object.keys(firstData)
    }

    
    const titres = (datasTitres) => {
        let listeTitres = []
        for (let i in datasTitres){
            if(datasTitres[i] != '')  {
                listeTitres[i] = datasTitres[i]
            }
            else{
                listeTitres[i] = ''
            }
        }
        //console.log(listeTitres)
        return listeTitres
    }

    const titresList = titres(datasTitres)
    //console.log(titresList)          

    const titresToShow = titresList.map((titre) =>
    <th key={titre}>{titre}</th>
    )
    
    const dataListe = (datas) => {
        const liste = []
        for(let i=0; i<datas.length; i++){
            liste.push(<tr></tr>)
            const valeurs = Object.values(datas[i])
            console.log(valeurs)
            
            const dataToShow = []
            
            for(let i=0; i<valeurs.length; i++){
        
                if(i === 0){
                    
                    dataToShow.push(<th key={valeurs[i]} scope="row">{valeurs[i]}</th>)
                    
                }
                else{
                    dataToShow.push(<td key={valeurs[i]}>{valeurs[i]}</td>)
                }
            } 
                        
            liste.push(dataToShow)
            liste.push(<tr></tr>)
        }
        return liste
    }

    const listeToShow = dataListe(datas)

    //console.log(titresToShow)
    console.log(listeToShow)

    

    

    tableau = <Table striped responsive>
                    <thead>
                        <tr>
                            { titresToShow }
                        </tr>
                    </thead>
                    <tbody>
                        { listeToShow }
                    </tbody>
                </Table>

    
    if(props.loggedIn && datas != []){
        return (
            <Aux>
                <OverlayTrigger overlay={<Tooltip>Load More</Tooltip>}>
                    <Button onClick={handleShowMore} variant={'outline-'+'primary'}><UcFirst text='more' /></Button>
                </OverlayTrigger>
                <Row>
                    <Col>
                        
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">{table.toUpperCase()}</Card.Title>
                                
                                </Card.Header>
                            <Card.Body>
                                { tableau }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                
            </Aux>
        );
    }     
    return (
        <div>Veuillez vous connecter</div>
    )      
    
    
    }

    const mapStateToProps = state => {
        return {
            loggedIn: state.loggedIn
        }
    };


export default connect(mapStateToProps)(GetData);