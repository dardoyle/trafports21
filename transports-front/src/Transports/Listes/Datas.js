import { data } from 'jquery'
import React from 'react'

const Datas = ({datasToRender}) => {
    return(
        <ul>
            {datasToRender.map((data, index) =>(
                <li key={index}>
                    <strong>{data.id}</strong>
                    &nbsp;{data.nom}
                </li>
            ))}
        </ul>
    )
}
export default Datas