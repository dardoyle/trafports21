import React, {Component} from 'react';
import {Row,
        Col,
        Button,
        Card,
        Table,
        OverlayTrigger,
        Tooltip,
        Form,
        FormControl
        } from 'react-bootstrap';
import UcFirst from "../../App/components/UcFirst"    
import {Redirect, Link, NavLink} from 'react-router-dom'
import Aux from "../../hoc/_Aux";
//import Card from "../../App/components/MainCard";

import apiClient from '../../services/api';
import { connect } from 'react-redux'
import * as actionTypes from "../../store/actions";
import retrieveDatas from '../../store/retrieveZones';
import axios from 'axios'
import Swal from 'sweetalert2'
import Loader from '../../App/layout/Loader'
import ExportCSV from '../../App/components/ExportCSV'

// nombre d'éléments affichés par page
const count = 10
let arrayToHoldData = []

const GetData = (props) => {
    
    const table = (props.table)
    const [datas, setDatas] = React.useState([])
    const [isLoading, setIsLoading] = React.useState(false);
    const [fileName, setFileName] = React.useState('datas')

       
    const retreiveDatas = () => {
        setIsLoading(true)
                     
            apiClient.get(`/api/${table}`)
                    
            .then(response => {
                setDatas(response.data)
                setIsLoading(false) 
            })
            .catch(error => console.log(error))
    } 

    React.useEffect(() => {
        if (props.loggedIn){
                retreiveDatas()
            } 
        }, [])

    
   
    

    const deleteData = (id) => {
        apiClient.delete(`/api/${table}/${id}`)
                    .then(response => {
                        Swal.fire({
                            title: 'Succès',
                            text: 'La base de données a bien été mise à jour',
                            icon: 'success'
                          }
                        )    
                    
                    })
                    .then (
                        retreiveDatas()
                    )
                    
                     
                .catch((error) => {
                    console.log(error)
                })
        
    }

    // conversion du nom en slug
    const convertToSlug = (Text) => {
        return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
    }

    let tableau = ''
    let datasTitres = []
    let firstData = datas[0]

    if(firstData != undefined){
        datasTitres = Object.keys(firstData)
    }

    
    const titres = (datasTitres) => {
        let listeTitres = []
        for (let i in datasTitres){
            if(datasTitres[i] != '')  {
                listeTitres[i] = datasTitres[i]
            }
            else{
                listeTitres[i] = ''
            }
        }
        
        return listeTitres
    }

    const titresList = titres(datasTitres)
             

    const titresToShow = titresList.map((titre) =>
    <th key={titre}>{titre}</th>
    )

    
   
    
    const dataListe = (datas) => {
        const liste = []
        for(let i=0; i<datas.length; i++){
            
            const valeurs = Object.values(datas[i])
                        
            const dataToShow = []
            
            for(let i=0; i<valeurs.length; i++){
        
                if(i === 1){
                    
                    
                    dataToShow.push(<tr key={valeurs[i]} scope="row">
                                        <td><NavLink to={{pathname: "/" + table + "/" + convertToSlug(valeurs[i]), dataProp: {valeurs}}} > {valeurs[i]} </NavLink></td>
                                        
                                        <td><NavLink to={{pathname: "/editer-"  + table + "/" + valeurs[0], dataProp: {valeurs}}} ><Button variant={'outline-primary'}><UcFirst text='éditer' /></Button></NavLink></td>
                                        <td><Button onClick={()=>deleteData(valeurs[0])} variant={'outline-primary'}><UcFirst text='supprimer' /></Button></td>
                                                                                
                                    </tr>)
                    
                }
                
            } 
                        
            liste.push(dataToShow)
            //liste.push(<tr></tr>)
        }
        return liste
    }

    const listeToShow = dataListe(datas)

    
 

    tableau = <Table striped responsive>
                    
                    <tbody>
                        { listeToShow }
                    </tbody>
                </Table>

    
    if(props.loggedIn && !isLoading){
        return (
            <Aux>
                
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title className='col-md-8' as="h5">{table.toUpperCase()}</Card.Title>
                                    
                                </Card.Header>
                            <Card.Body>
                                { tableau }
                                
                            </Card.Body>
                            <Card.Footer>
                                <ExportCSV className='col-md-4 center' csvData={datas} fileName={fileName}/>
                            </Card.Footer>
                        </Card>
                    </Col>
                    
                </Row>
                
            </Aux>
        );
    }  
    else if(props.loggedIn && isLoading){
        return(
            <Loader/>
            
        )
    }   
    else{
        return (
            <div>Veuillez vous connecter</div>
        )
    }
    
          
    }

    const mapStateToProps = state => {
        return {
            loggedIn: state.bases.loggedIn
        }
    };

     // à ne laisser que si utilisation redux pour rappatriement des données
     // tester avec le nom de la table en paramètre
    const mapDispatchToProps = dispatch => {
        return{
            retrieveDatas: () => dispatch(retrieveDatas())
        }
    }


export default connect(mapStateToProps, mapDispatchToProps)(GetData);