import React, {Component} from 'react';
import {Row, Col, Card, Table} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";
//import Card from "../../App/components/MainCard";

import apiClient from '../../services/api';
import { connect } from 'react-redux'

const Ports = (props) => {
    
    const [ports, setPorts] = React.useState([]);
    
    
    React.useEffect(() => {
        if (props.loggedIn){
            apiClient.get('/api/ports')
            .then(response => {
                setPorts(response.data)
                
        
            })
            .catch(error => console.log(error))
            }
          }, [])

            console.log(ports)          
          const portsListe = ports.map((port) =>
            <tr key={port.id}>
                <td>#</td>
                <td>{port.nomFr}</td>
                
            </tr>
          )
               
    
        
        if(props.loggedIn && ports != []){
            return (
                <Aux>
                    <Row>
                        <Col>
                            
                            <Card>
                                <Card.Header>
                                    <Card.Title as="h5">PORTS</Card.Title>
                                    
                                </Card.Header>
                                <Card.Body>
                                    <Table striped responsive>
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nom</th>
                                            
                                        </tr>
                                        {portsListe}
                                        </thead>
                                        <tbody>
    
                                        </tbody>
                                    </Table>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Aux>
            );
        }     
        return (
            <div>Veuillez vous connecter</div>
        )      
        
        
        }

        const mapStateToProps = state => {
            return {
                loggedIn: state.loggedIn
            }
        };


export default connect(mapStateToProps)(Ports);