import React, { useEffect } from 'react';
import {Row,
        Col,
        Button,
        Card,
        Table,
        
        } from 'react-bootstrap';
import UcFirst from "../../App/components/UcFirst"    
import {NavLink} from 'react-router-dom'
import Aux from "../../hoc/_Aux";
//import Card from "../../App/components/MainCard";

import apiClient from '../../services/api';
// connect est un HOC (High Order Component qui va permettre de passer le state 
// et les dispatch(action) en props au composant) - connect est une fonction,
// raison pour laquelle elle est entre accolades
import { connect } from 'react-redux'
import { apiDatasCall } from '../../store/datas/actionData'
//import {retrieveZonesSuccess} from '../../store/zones/actionZone';

import Swal from 'sweetalert2'
import Loader from '../../App/layout/Loader'
import ExportCSV from '../../App/components/ExportCSV'

// nombre d'éléments affichés par page
const count = 10
let arrayToHoldData = []


const GetData = (props) => {
    // ne le faire que si un truc du style state.datas[props.table] n'existe pas
    //console.log('l33 ' + props.table)
    //console.log(props.apiData)
    /* if (props.zones){
        for (let i=0; i<props.zones.length; i++){
            console.log( 'l 37 ' + Object.values(props.zones[i]))
        } 
        
    } */
    /* if (props.entreprises){
        console.log( 'l 39 ' + props.entreprises)
    } */
        

    /* props.contrats ? console.log('l 39 ' + props.contrats) : ''
    props.entreprises ? console.log('l 40 ' + props.entreprises) : '' */
    
    useEffect(() => {
        props.apiDatasDispatch()        
    }, [props.apiDatas])

    /* useEffect(() => {
        props.apiDatasDispatchEntreprises()        
    }, [props.apiDatasEntreprises]) */

    
    //const [datas, setDatas] = React.useState([])
    const [fileName, setFileName] = React.useState('datas')
   
    const deleteData = (id) => {
        apiClient.delete(`/api/${props.table}/${id}`)
                    .then(response => {
                        Swal.fire({
                            title: 'Succès',
                            text: 'La base de données a bien été mise à jour',
                            icon: 'success'
                          }
                        )    
                    
                    })
                    .then (
                        props.apiDatasDispatch()
                    )
                    
                     
                .catch((error) => {
                    console.log(error)
                })
        
    }

    // conversion du nom en slug
    const convertToSlug = (Text) => {
        return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
    }

    
      
    let tableau = ''
    
    
    if(props.apiData != undefined){
        
        let apiData = props.apiData
        
        const dataListe = (apiData) => {
            const liste = []
        
            for(let i=0; i<apiData.length; i++){
                
                const valeurs = Object.values(apiData[i])
                
                const dataToShow = []
                
                for(let i=0; i<valeurs.length; i++){
            
                    if(i === 1){
                        
                        props.table === 'contrats' ?
                            dataToShow.push(<tr key={valeurs[i]} scope="row">
                                                <td><NavLink to={{pathname: "/" + props.table + "/" + convertToSlug(valeurs[i]), dataProp: {valeurs}}} > {valeurs[i]} </NavLink></td>
                                                
                                                <td><NavLink to={{pathname: "/editer-"  + props.table + "/" + valeurs[0], dataProp: {valeurs}}} ><Button variant={'outline-primary'}><UcFirst text='éditer' /></Button></NavLink></td>
                                                <td><Button onClick={()=>deleteData(valeurs[0])} variant={'outline-primary'}><UcFirst text='supprimer' /></Button></td>
                                                                                        
                                            </tr>)  :
                        props.table === 'zones' || props.table === 'marchandises'  || props.table === 'nsts' || props.table === 'releves' || props.table === 'ports'?
                                    dataToShow.push(<tr key={valeurs[i]} scope="row">
                                    <td> {valeurs[i]} </td>
                                    
                                    
                                    <td><Button onClick={()=>deleteData(valeurs[0])} variant={'outline-primary'}><UcFirst text='supprimer' /></Button></td>
                                                                            
                                </tr>)           
                                                    :

                            dataToShow.push(<tr key={valeurs[i]} scope="row">
                            <td><NavLink to={{pathname: "/" + props.table + "/" + convertToSlug(valeurs[i]), dataProp: {valeurs}}} > {valeurs[i]} </NavLink></td>
                            
                            
                            <td><Button onClick={()=>deleteData(valeurs[0])} variant={'outline-primary'}><UcFirst text='supprimer' /></Button></td>
                                                                     
                         </tr>)               
                          
                    }
                    
                } 
                            
                liste.push(dataToShow)
                
            }
            return liste
        }
    
        const listeToShow = dataListe(props.apiData)

        tableau = <Table striped responsive>
                    
                    <tbody>
                        { listeToShow }
                    </tbody>
                </Table>
    }
       

    
    if(props.loggedIn && !props.isLoading){
        return (
            <Aux>
                
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title className='col-md-8' as="h5">{props.table.toUpperCase()}</Card.Title>
                                    
                                </Card.Header>
                            <Card.Body>
                                { tableau }
                                
                            </Card.Body>
                            <Card.Footer>
                                <ExportCSV className='col-md-4 center' csvData={props.apiData} fileName={fileName}/>
                            </Card.Footer>
                        </Card>
                    </Col>
                    
                </Row>
                
            </Aux>
        );
    }  
    else if(props.loggedIn && props.isLoading){
        return(
            <Loader/>
            
        )
    }   
    else{
        return (
            <div>Veuillez vous connecter</div>
        )
    } 
        
            
        
          
    }

    const mapStateToProps = (state, ownProps) => {
        
        return {
            table: ownProps.table,
            loggedIn: state.bases.loggedIn,
            apiData: state.datas[ownProps.table],
            
            //isLoading: state.datas.isLoading
        }
    };

     // à ne laisser que si utilisation redux pour rappatriement des données
     // tester avec le nom de la table en paramètre
    const mapDispatchToProps = (dispatch,ownProps) => {
        //console.log(ownProps.table)
        return{
            apiDatasDispatch: () => dispatch(apiDatasCall(ownProps.table))
            
        }
                
        
    }


export default connect(mapStateToProps, mapDispatchToProps)(GetData);