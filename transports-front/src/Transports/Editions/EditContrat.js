import React, { Component } from 'react'
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';
import UcFirst from "../../App/components/UcFirst"
import axios from 'axios'
import Swal from 'sweetalert2'

import apiClient from '../../services/api';
import { connect } from 'react-redux'
import * as actionTypes from "../../store/actions";
import Aux from "../../hoc/_Aux";

import { Redirect } from 'react-router'

import Select from 'react-select'

class EditContrat extends Component {
    constructor(props){
        super(props)
        
        // mise en place des fonctions
        this.handleChange = this.handleChange.bind(this)

        // mise en place du state
        
        this.state = {
            //listeEntreprises: [],
            //entreprises: [],
            //idEntr: '',
            //ports: [],
            entreprise: '',  // id de l'entreprise qui est sélectionnée par défaut car dans la db
            //nomEntrepriseActive : '',
            port: '',        // id du port   
            //idPort: '',
            //selectEntrOptions: [],
            //selectedEntr: '',
            //idEntr: '',
            //nomEntr: '',
            //selectEntrOptions: '',
            //selectPortOptions: [],
            //idPort: '',
            //nomPort: '',
            refPolice: '',
            refCompta: '',
            remarque: '',
            foisDeux: false,
            success: false
        }
    }

    /**
     * 
     * @param {*} table table qui correspond à la clé étrangère pour laquelle on veut récupérer la liste pour alimenter le select 
     * @param {*} tableOptions élément du state qui contiendra la liste
     */
    getOptions = (table, tableOptions) => {
        // console.log('l 51 ' + tableOptions)     
        apiClient.get('/api/' + table)
    
        .then(response => {
           // console.log('l 55 ' + tableOptions)
            const data = response.data
             
            const options = data.map(item => ({
                'value' : item.id,
                'label' : item.nom
            }))
            
           //console.log('l 69 ' + options)
            
            this.setState({selectEntrOptions: options})
                      // console.log(this.state.selectPortOptions)
          
             
        })
        .catch(error => console.log(error))
    }


    /**
     * 
     * @param {*} table table qui correspond à la clé étrangère pour laquelle on veut récupérer la liste pour alimenter le select 
     * @param {*} tableOptions élément du state qui contiendra la liste
     */
     getOptionsPorts = (table, tableOptions) => {
        // console.log('l 51 ' + tableOptions)     
        apiClient.get('/api/indexBE')
    
        .then(response => {
           // console.log('l 55 ' + tableOptions)
            const data = response.data
             
            const listePorts = Object.values(data)
            const options = listePorts.map(item => ({
                'value' : item.id,
                'label' : item.nomFr
            }))
            
          // onsole.log('l 69 ' + options)
           
            this.setState({selectPortOptions: options})
           // console.log(this.state.selectPortOptions)
          
             
        })
        .catch(error => console.log(error))
    }

    componentDidMount(){
            
        
        apiClient.get('/api/contrats/' + this.props.match.params.id)
                .then(response =>{ 
                    
                    this.setState({
                        entreprise: response.data.entreprise_id,
                        port: response.data.ville_id,
                        refPolice: response.data.refPolice,
                        refCompta: response.data.refCompta,
                        remarque: response.data.remarque,
                        foisDeux: response.data.foisDeux,
                        dateDeb: response.data.dateDeb,
                        dateFin: response.data.dateFin
                        })
                    })
                .catch((error) => {
                    console.log(error)
                })

        
        this.getOptions('entreprises', 'selectEntrOptions')
        this.getOptionsPorts('ports', 'selectPortOptions')
       }

        
        

    handleChange = (e) => {
        //console.log(e.target.name + ' ' + e.target.value);
        //console.log('handleChange ' + e.target.value)
        this.setState({
            // on va chercher l'id de l'élément sur lequel il y a un chgt, on prend la valeur et on va la mettre dans le state
            
            [e.target.name]: e.target.value

        }) 
       // console.log(this.state.entreprise)
    }

    handleChangeSelectEntr = (e) => {
       // console.log(e)
        this.setState({entreprise: e.value})
    }

    handleChangeSelectPort = (e) => {
        //console.log(e)
        this.setState({port: e.value})
    }

    handleSubmit = (e) => {
        e.preventDefault()
        //console.log('handleSubmit ' + this.state.entreprise)

        const contrat = {
            
            refPolice : this.state.refPolice,
            refCompta : this.state.refCompta,
            remarque : this.state.remarque,
            foisDeux : this.state.foisDeux,
            dateDeb : this.state.dateDeb,
            dateFin : this.state.dateFin,
            ville_id: this.state.port,
            entreprise_id: this.state.entreprise,
        }

        if (this.props.loggedIn){
            
            apiClient.put('/api/contrats/' + this.props.match.params.id, contrat)
                .then(response => {
                    // console.log('coucou')
                    Swal.fire(
                        'Le contrat a bien été mis à jour'
                    )
                })
                .catch((error) => {
                    console.log(error)
                })
        }

        // Redirection vers la liste
        this.props.history.push('/contrats')
    }

    /**
     * 
     * @param {*} object liste des options du select  
     * @param {*} value  identifiant du default select
     * @returns  le nom du default select
     */
    getDefault = (object, value) => {
        if (object != undefined){
            const tab = object
            const defaultVal = tab.filter(function(val){
                return val.value == value
        })
        
            const defaultValue = defaultVal[0].label
                return defaultValue
        
        
        }
    }

    
    
    render(){
        /* const selected = this.state.selectEntrOptions.filter(function(elt){
            return elt.id === this.state.entreprise
        })  
        console.log(selected) */
        //console.log('l 191 ' + this.state.selectPortOptions)

          if (this.props.loggedIn && !this.state.success){
                  
            const defaultEntr = this.getDefault(this.state.selectEntrOptions, this.state.entreprise)
            const defaultPort = this.getDefault(this.state.selectPortOptions, this.state.port)
                   
            
           // console.log(this.state.entreprise + ' ' + this.state.port)
            return (
                <Aux>
                    <Row>
                        <Col>
                            <Card>
                                <Card.Header>
                                    <Card.Title as="h5">Modification d'un contrat</Card.Title>
                                </Card.Header>
                                <Card.Body>
                                
                                    <Row>
                                        <Col md={6}>
                                            <Form onSubmit={ this.handleSubmit }>
                                                <Form.Group controlId="Nom">
                                                    <Form.Label>Entreprise</Form.Label>
                                                    
                                                    <Select                                        
                                                        value={{value: this.state.entreprise, label: defaultEntr}}
                                                        options={this.state.selectEntrOptions} 
                                                        onChange={ this.handleChangeSelectEntr }/>
                                                    
                                                    
                                                    <Form.Label>Port</Form.Label>
                                                    <Select className="selectF"
                                                        value={{value: this.state.port, label: defaultPort}}
                                                        options={this.state.selectPortOptions} 
                                                        onChange={ this.handleChangeSelectPort }/>         
                                                    
                                                    <Form.Label>Référence police</Form.Label>
                                                    <Form.Control type="text" name='refPolice' value={ this.state.refPolice } onChange= { this.handleChange } />
                                                    <Form.Label>Référence compta</Form.Label>
                                                    <Form.Control type="text" name='refCompta' value={ this.state.refCompta } onChange= { this.handleChange } />
                                                    <Form.Label>Remarque</Form.Label>
                                                    <Form.Control type="text" name='remarque' value={ this.state.remarque } onChange= { this.handleChange } />
                                                    <Form.Label>Fois deux</Form.Label>
                                                    <Form.Control type="text" name='foisDeux' value={ this.state.foisDeux } onChange= { this.handleChange } />
                                                </Form.Group>
    
                                                <Button type="submit" variant={'outline-primary'}><UcFirst text='modifier le contrat' /></Button>
                                                    
                                            
                                            </Form>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    
                </Aux>
            )
    
        }
        else if (this.props.loggedIn && this.state.success) {
            return <Redirect to='/Contrats'/>
        }
        else {
            return <h4>Veuillez vous connecter</h4>
        }
      
    }
}

const mapStateToProps = state => {
    return {
        loggedIn: state.bases.loggedIn
    }
};

    

export default connect(mapStateToProps)(EditContrat);