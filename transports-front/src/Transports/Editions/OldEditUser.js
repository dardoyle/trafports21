import React, { Component } from 'react'
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';
import UcFirst from "../../App/components/UcFirst"
import Swal from 'sweetalert2'

import apiClient from '../../services/api';
import { connect } from 'react-redux'
import Aux from "../../hoc/_Aux";

import { Redirect } from 'react-router'

import Select from 'react-select'

class EditUser extends Component {
    constructor(props){
        super(props)
        
        // mise en place des fonctions
        this.handleChange = this.handleChange.bind(this)

        // mise en place du state
        
        this.state = {
            user: '',  // id de l'utilisateur qui est sélectionné par défaut 
            name: '',
            email: '',
            password: ''
        }
    }

    
    componentDidMount(){
        apiClient.get('/api/users/' + this.props.match.params.id)
                .then(response =>{ 
                    
                    this.setState({
                        user: response.data.user_id,
                        name: response.data.name,
                        email: response.data.email,
                        password: response.data.password,
                        })
                    })
                .catch((error) => {
                    console.log(error)
                })
        }

        
    handleChange = (e) => {
        this.setState({
            // on va chercher l'id de l'élément sur lequel il y a un chgt, on prend la valeur et on va la mettre dans le state
            
            [e.target.name]: e.target.value

        }) 
       
    }

    handleChangeSelectEntr = (e) => {
       // console.log(e)
        this.setState({entreprise: e.value})
    }

    handleChangeSelectPort = (e) => {
        //console.log(e)
        this.setState({port: e.value})
    }

    handleSubmit = (e) => {
        e.preventDefault()
        //console.log('handleSubmit ' + this.state.entreprise)

        const user = {
            
            user : this.state.id,
            name : this.state.name,
            email : this.state.email,
            password : this.state.password,
            
        }

        if (this.props.loggedIn){
            
            apiClient.put('/api/users/' + this.props.match.params.id, user)
                .then(response => {
                    // console.log('coucou')
                    Swal.fire(
                        'L\'utilisateur a bien été mis à jour'
                    )
                })
                .catch((error) => {
                    console.log(error)
                })
        }

        // Redirection vers la liste
        this.props.history.push('/users')
    }

    
    
    
    render(){
        if (this.props.loggedIn && !this.state.success){
            return (
                <Aux>
                    <Row>
                        <Col>
                            <Card>
                                <Card.Header>
                                    <Card.Title as="h5">Modification d'un utilisateur</Card.Title>
                                </Card.Header>
                                <Card.Body>
                                
                                    <Row>
                                        <Col md={6}>
                                            <Form onSubmit={ this.handleSubmit }>
                                                <Form.Group controlId="Nom">
                                                    <Form.Label>Utilisateur</Form.Label>
                                                    <Form.Label>Nom</Form.Label>
                                                    <Form.Control type="text" name='name' value={ this.state.name } onChange= { this.handleChange } />
                                                    <Form.Label>Email</Form.Label>
                                                    <Form.Control type="text" name='email' value={ this.state.email } onChange= { this.handleChange } />
                                                    <Form.Label>Password</Form.Label>
                                                    <Form.Control type="text" name='email' value={ this.state.email } onChange= { this.handleChange } />
                                                </Form.Group>
    
                                                <Button type="submit" variant={'outline-primary'}><UcFirst text='modifier le contrat' /></Button>
                                                    
                                            
                                            </Form>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    
                </Aux>
            )
    
        }
        else if (this.props.loggedIn && this.state.success) {
            return <Redirect to='/Contrats'/>
        }
        else {
            return <h4>Veuillez vous connecter</h4>
        }
      
    }
}

const mapStateToProps = state => {
    return {
        loggedIn: state.bases.loggedIn
    }
};

    

export default connect(mapStateToProps)(EditContrat);