import React, { Component } from 'react'
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';
import UcFirst from "../../App/components/UcFirst"
import axios from 'axios'
import Swal from 'sweetalert2'

import apiClient from '../../services/api';
import { connect } from 'react-redux'
import * as actionTypes from "../../store/actions";
import Aux from "../../hoc/_Aux";

import { Redirect } from 'react-router'

class EditContrat extends Component {
    constructor(props){
        super(props)
        
        // mise en place des fonctions
        this.handleChange = this.handleChange.bind(this)

        // mise en place du state
        
        this.state = {
            //listeEntreprises: [],
            entreprises: [],
            //idEntr: '',
            ports: [],
            entreprise: '',  // id de l'entreprise
            nomEntrepriseActive : '',
            port: '',        // id du port   
            //idPort: '',
            refPolice: '',
            refCompta: '',
            remarque: '',
            foisDeux: false,
            success: false
        }
    }

    componentDidMount(){
            
        
        apiClient.get('/api/contrats/' + this.props.match.params.id)
                .then(response =>{ 
                    
                    this.setState({
                        entreprise: response.data.entreprise_id,
                        port: response.data.ville_id,
                        refPolice: response.data.refPolice,
                        refCompta: response.data.refCompta,
                        remarque: response.data.remarque,
                        foisDeux: response.data.foisDeux,
                        dateDeb: response.data.dateDeb,
                        dateFin: response.data.dateFin
                        })
                    })
                .catch((error) => {
                    console.log(error)
                })

        apiClient.get('/api/entreprises')
    
        .then(response => {
            let nomsEntreprises = []
            let idEntreprise = ''
            
            this.setState({listeEntreprises: response.data})
            console.log('liste entreprises ' + this.state)
            this.state.listeEntreprises.map((entreprise) => {
                nomsEntreprises.push(entreprise.nom)
                idEntreprise = entreprise.id
                
            })
            this.setState({ entreprises: nomsEntreprises })
            
            this.state.listeEntreprises.map((entreprise) => {
                if(entreprise.id === this.state.entreprise){
                    this.setState({nomEntrepriseActive: entreprise.nom})
                }
                
                
            })
            
        })
        .catch(error => console.log(error))
        

        apiClient.get('/api/indexBE')
                
        .then(response => {
            
            let listePorts = []
            let nomsPorts = []
            let idPort = ''
            
            listePorts = response.data
            
            listePorts = Object.values(listePorts)

            this.setState({ listePorts: listePorts })
            
            listePorts.map((port) => {
                nomsPorts.push(port.id + '    ' + port.nomFr)
                idPort = port.id
                this.setState({ idPort: idPort })
            }) 

            this.setState({ ports: nomsPorts })
            this.setState({ port: idPort})
            //console.log(this.state.ports)
        })
        .catch(error => console.log(error))
        
        }

        
    
      

    handleChange = (e) => {
        //console.log(e.target.name + ' ' + e.target.value);
        console.log('handleChange ' + e.target.value)
        this.setState({
            // on va chercher l'id de l'élément sur lequel il y a un chgt, on prend la valeur et on va la mettre dans le state
            
            [e.target.name]: e.target.value

        }) 
        console.log(this.state.entreprise)
    }

    handleSubmit = (e) => {
        e.preventDefault()
        console.log('handleSubmit ' + this.state.entreprise)

        const contrat = {
            
            refPolice : this.state.refPolice,
            refCompta : this.state.refCompta,
            remarque : this.state.remarque,
            foisDeux : this.state.foisDeux,
            dateDeb : this.state.dateDeb,
            dateFin : this.state.dateFin,
            ville_id: this.state.idPort,
            entreprise_id: this.state.idEntr,
        }

        if (this.props.loggedIn){
            
            apiClient.put('/api/contrats/' + this.props.match.params.id, contrat)
                .then(response => {
                    // console.log('coucou')
                    Swal.fire(
                        'Le contrat a bien été mis à jour'
                    )
                })
                .catch((error) => {
                    console.log(error)
                })
        }

        // Redirection vers la liste
        this.props.history.push('/contrats')
    }

    
    render(){
        console.log('avant render ' + this.state.listeEntreprises)    
        const entreprises = this.state.entreprises.map((entreprise, index) => {
            return <option name='entreprise' key={ index }>{ entreprise }</option>
        })
        const ports = this.state.ports.map((port, index) => {
            return <option name='port' key={ index }>{ port }</option>
        })
        if (this.props.loggedIn && !this.state.success){
           // console.log(this.state.entreprise + ' ' + this.state.port)
            return (
                <Aux>
                    <Row>
                        <Col>
                            <Card>
                                <Card.Header>
                                    <Card.Title as="h5">Modification d'un contrat</Card.Title>
                                </Card.Header>
                                <Card.Body>
                                
                                    <Row>
                                        <Col md={6}>
                                            <Form onSubmit={ this.handleSubmit }>
                                                <Form.Group controlId="Nom">
                                                    <Form.Label>Entreprise</Form.Label>
                                                    <Form.Control as="select" className="mb-3" name='entreprise' value={this.state.entreprise} onChange={ this.handleChange }>
                                                        <option>{this.state.entreprise + ' - ' + this.state.nomEntrepriseActive}</option>
                                                        { entreprises }         
                                                    </Form.Control>
                                                    <Form.Label>Port</Form.Label>
                                                    <Form.Control as="select" className="mb-3" name='port' value={this.state.port} onChange={ this.handleChange }>
                                                        <option>{this.state.port}</option>
                                                        { ports }         
                                                    </Form.Control>
                                                    <Form.Label>Référence police</Form.Label>
                                                    <Form.Control type="text" name='refPolice' value={ this.state.refPolice } onChange= { this.handleChange } />
                                                    <Form.Label>Référence compta</Form.Label>
                                                    <Form.Control type="text" name='refCompta' value={ this.state.refCompta } onChange= { this.handleChange } />
                                                    <Form.Label>Remarque</Form.Label>
                                                    <Form.Control type="text" name='remarque' value={ this.state.remarque } onChange= { this.handleChange } />
                                                    <Form.Label>Fois deux</Form.Label>
                                                    <Form.Control type="text" name='foisDeux' value={ this.state.foisDeux } onChange= { this.handleChange } />
                                                </Form.Group>
    
                                                <Button type="submit" variant={'outline-primary'}><UcFirst text='modifier le contrat' /></Button>
                                                    
                                            
                                            </Form>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    
                </Aux>
            )
    
        }
        else if (this.props.loggedIn && this.state.success) {
            return <Redirect to='/Contrats'/>
        }
        else {
            return <h4>Veuillez vous connecter</h4>
        }
        
    }
}

const mapStateToProps = state => {
    return {
        loggedIn: state.loggedIn
    }
};

    

export default connect(mapStateToProps)(EditContrat);