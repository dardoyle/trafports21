import React, {Component} from 'react';
import {Row, Col, Card, Table} from 'react-bootstrap';

import Aux from "../hoc/_Aux";
//import Card from "../../App/components/MainCard";

import apiClient from '../services/api';
import { connect } from 'react-redux'

const Zones = (props) => {
    //console.log(' zones ' + props.loggedIn)
    const [zones, setZones] = React.useState([]);
    
    React.useEffect(() => {
        if (props.loggedIn){
            apiClient.get('/api/zones')
            .then(response => {
                setZones(response.data)
                //console.log('zones ' + response.data)
        
            })
            .catch(error => console.log(error))
            }
          }, [])

          //console.log(zones)

          let tableau = ''
          let zonesTitres = []
          
          let firstZone = zones[0]
                 
          console.log(firstZone)
          if (firstZone != undefined){
            zonesTitres = Object.keys(firstZone)
            console.log(zonesTitres)
          }
          
          const titres = (zonesTitres) => {
                let listeTitres = []
                for (let i in zonesTitres){
                    if(zonesTitres[i] != 'id' && zonesTitres[i] != 'updated_at'){
                        listeTitres[i] = zonesTitres[i]
                    }
                    else{
                        listeTitres[i] = ''
                    }
                }
                console.log(listeTitres)
                return listeTitres
          }

          const titresList = titres(zonesTitres)
          console.log(titresList)          

          const titresToShow = titresList.map((titre) =>
           <th key={titre}>{titre}</th>
          )

          /* const titres = zonesTitres.map((titre) =>
            <th key={titre}>{titre}</th>
          ) */
          
          const zonesListe = zones.map((zone) =>
            <tr key={zone.id}>
                <th scope ="row">#</th>
                <td>{zone.nom}</td>
                <td>{zone.created_at}</td>
            </tr>
          )
           

          tableau = <Table striped responsive>
                        <thead>
                        <tr>
                            {titresToShow}
                        </tr>
                        
                        </thead>
                        <tbody>
                            
                            {zonesListe}
                        </tbody>
                    </Table>
              
    
        
        if(props.loggedIn && zones != []){
            return (
                <Aux>
                    <Row>
                        <Col>
                            
                            <Card>
                                <Card.Header>
                                    <Card.Title as="h5">Zones</Card.Title>
                                    
                                </Card.Header>
                                <Card.Body>
                                    {tableau}
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Aux>
            );
        }     
        return (
            <div>Veuillez vous connecter</div>
        )      
        
        
        }

        const mapStateToProps = state => {
            return {
                loggedIn: state.loggedIn
            }
        };


export default connect(mapStateToProps)(Zones);