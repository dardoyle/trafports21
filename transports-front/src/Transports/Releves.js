import React from 'react';
import { Redirect } from 'react-router-dom'

import './../../../assets/scss/style.scss';
import {connect} from 'react-redux';
import * as actionTypes from "../../../store/actions";
/* import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb"; */

//import axios from 'axios';
import apiClient from '../../../services/api';

import Cookies from 'js-cookie';
//import cookie from 'cookie'

import AdminLayout from '../../../App/layout/AdminLayout'
import Dashboard from '../../../Demo/Dashboard/Default'



const Transports = ({role, applyRole}) => {
    
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    //const [roleUser, setRoleUser] = React.useState('');
    const [message, setMessage] = React.useState('');

        

    const handleSubmit = (e) => {
        e.preventDefault();
        apiClient.get('/sanctum/csrf-cookie')
    .then(response => {
        apiClient.post('api/login',{
            email: email,
            password: password
        }).then(response => {
    
            if(response.data.error){
                console.log(response.data.error)
                
            } else{
                console.log(response.data)
                Cookies.set('ticket_management_is_user_logged_in', true, {expires: 86400, sameSite: 'lax'})
                applyRole(response.data.data.role[0])
                //let roleUser = response.data.data.role
                //console.log('signin l 38 ' + response.data.data.role)
                //props.history.push('/dashboard/default');
                
            }
            
        }).catch(error => {
            console.log('utilisateur non autorisé')
            setEmail('')
            setPassword('')
            setMessage('erreur dans l\'adresse mail ou le mot de passe')
                    })
    });
    }

        
    //console.log('valeur rôle ' + role)
    if (role != 'guest') {
        
        return(
            <Redirect to="/dashboard/default" component={Dashboard}/>
            /* <AdminLayout>
                <Dashboard />
            </AdminLayout>
             */

        )
        
    }
    else {
        return (
            
            /* <Aux>
                    <Breadcrumb/> */
                <>
                    {/* <Image src="assets/images/logoTransports.png" alt='logo' /> */}
                    
                    <div className="auth-wrapper">
                    
                        <div className="auth-content">
                            <div className="auth-bg">
                                <span className="r"/>
                                <span className="r s"/>
                                <span className="r s"/>
                                <span className="r"/>
                            </div>
                            <div className="card">
                                <div className="card-body text-center">
                                    <div className="mb-4">
                                        <i className="feather icon-unlock auth-icon"/>
                                    </div>
                                    
                                    <h3 className="mb-4">Login</h3>
                                    <form onSubmit={handleSubmit}>                                    
                                        <div className="input-group mb-3">
                                            <input type="email" name="email" value={email} onChange={e => setEmail(e.target.value)} className="form-control" placeholder="Email"/>
                                        </div>
                                        <div className="input-group mb-4">
                                            <input type="password" name="password" value={password} onChange={e => setPassword(e.target.value)} className="form-control" placeholder="password"/>
                                        </div>
                                        <div className="input-group mb-4">
                                            <input type="hidden" name="role" defaultValue={role} className="form-control" placeholder={role}/>
                                        </div>
                                        {/* <div className="form-group text-left">
                                            <div className="checkbox checkbox-fill d-inline">
                                                <input type="checkbox" name="checkbox-fill-1" id="checkbox-fill-a1" />
                                                    <label htmlFor="checkbox-fill-a1" className="cr"> Save credentials</label>
                                            </div>
                                        </div> */}
                                        <button className="btn btn-primary shadow-2 mb-4">Login</button>
                                        {/* <p className="mb-2 text-muted">Forgot password? <NavLink to="/auth/reset-password-1">Reset</NavLink></p> */}
                                        {/* <p className="mb-0 text-muted">Don’t have an account? <NavLink to="/auth/signup-1">Signup</NavLink></p> */}
                                    </form>
                                    
                                    {message ? <div class="alert alert-warning" role="alert">{message}</div> : ''}
 
                                </div>
                            </div>
                        </div>
                    </div>
                </>
                /* </Aux> */
        );
    }
}
 
const mapStateToProps = state => {
    return {
        role: state.bases.role
    }
};

const mapDispatchToProps = dispatch => {
    return {
        applyRole: (role) => dispatch({type: actionTypes.APPLY_ROLE, role}),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn1);