import React, {useState, useEffect} from 'react';
import {Row, Col, Card, Table} from 'react-bootstrap';
import * as moment from 'moment'

const User = (props) => {
    
    
    let valeurs = ''
    props.location.dataProp != null ?
        valeurs = Object.values(props.location.dataProp)
                                :
        valeurs = ''

    if(valeurs != ''){
    
        return (
            
            <Card>
                <Card.Header>
                    <Card.Title as="h5">Détails concernant l'utilisateur</Card.Title>
                    
                </Card.Header>
                <Card.Body>
                <Table striped responsive>
                    <tbody>
                        <tr>
                            <th scope="row">Nom</th>
                            
                            <td>{ valeurs[0][1]}</td> 
                        </tr>
                        <tr>
                            <th scope="row">Email</th>
                            <td>{ valeurs[0][2] }</td>
                        </tr>
                                            
                    </tbody>    
                </Table>
                        
                </Card.Body>
            </Card>
        )
    }
    else
    {
        // problème à résoudre - visiblement lié au basename mais pas encore trouvé le moyen de le résoudre
        return <p>Faites votre choix dans le menu de gauche</p>
    }
}

export default User
