import React from 'react';
import {Card, Table} from 'react-bootstrap';
import * as moment from 'moment'

const Entreprise = (props) => {
    console.log('props entreprise ' + props.location)
    let valeurs = ''
    props.location.dataProp != null ?
        valeurs = Object.values(props.location.dataProp)
                                :
        valeurs = ''

    if(valeurs != ''){
        return (
        
        <Card>
            <Card.Header>
                <Card.Title as="h5">Détails de l'entreprise</Card.Title>
                
            </Card.Header>
            <Card.Body>
            <Table striped responsive>
                <tbody>
                    <tr>
                        <th scope="row">Nom</th>
                        <td>{ valeurs[0][1] }</td> 
                    </tr>
                    <tr>
                        <th scope="row">Adresse</th>
                        <td>{ valeurs[0][2] }</td>
                    </tr>
                    <tr>
                        <th scope="row">Téléphone</th>
                        <td>{ valeurs[0][3] }</td>
                    </tr>
                    <tr>
                        <th scope="row">Mail</th>
                        <td>{ valeurs[0][4] }</td>
                    </tr>
                </tbody>    
            </Table>
                    
            </Card.Body>
        </Card>
    )
        }
        else{
            // problème à résoudre - visiblement lié au basename mais pas encore trouvé le moyen de le résoudre
            return <p>Faites votre choix dans le menu de gauche</p>
        }
}

export default Entreprise
