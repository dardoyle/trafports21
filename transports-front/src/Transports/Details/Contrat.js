import React, {useState, useEffect} from 'react';
import {Row, Col, Card, Table} from 'react-bootstrap';
import * as moment from 'moment'

const Contrat = (props) => {
    
    let valeurs = ''
    props.location.dataProp != null ?
        valeurs = Object.values(props.location.dataProp)
                                        :
        valeurs = ''

    if(valeurs != ''){
        let dateDeb = valeurs[0][5]
        dateDeb = moment(dateDeb).utc().format('DD-MM-YYYY')
        let dateFin = valeurs[0][6]
        dateFin = moment(dateFin).utc().format('DD-MM-YYYY')
        return (
            
            <Card>
                <Card.Header>
                    <Card.Title as="h5">Détails du contrat</Card.Title>
                    
                </Card.Header>
                <Card.Body>
                <Table striped responsive>
                    <tbody>
                        <tr>
                            <th scope="row">Concessionnaire</th>
                            <td>{ valeurs[0][9].nom}</td>
                        </tr>
                        <tr>
                            <th scope="row">Port</th>
                            <td>{ valeurs[0][10].nomFr}</td>
                        </tr>
                        <tr>
                            <th scope="row">Référence police</th>
                            <td>{ valeurs[0][1] }</td>
                        </tr>
                        <tr>
                            <th scope="row">Référence compta</th>
                            <td>{ valeurs[0][2] }</td>
                        </tr>
                        <tr>
                            <th scope="row">Remarque</th>
                            <td>{ valeurs[0][3] }</td>
                        </tr>
                        <tr>
                            <th scope="row">Date de début</th>
                            <td>{ dateDeb }</td>
                        </tr>
                        <tr>
                            <th scope="row">Date de fin</th>
                            <td>{ dateFin }</td>
                        </tr>
                    </tbody>    
                </Table>
                        
                </Card.Body>
            </Card>
        )
    }
    else{
            // problème à résoudre - visiblement lié au basename mais pas encore trouvé le moyen de le résoudre
            return <p>Faites votre choix dans le menu de gauche</p>
    }
}

export default Contrat
