import React, { Component } from 'react'
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';
import UcFirst from "../../App/components/UcFirst"
import axios from 'axios'
import Swal from 'sweetalert2'

import apiClient from '../../services/api';
import { connect } from 'react-redux'
import * as actionTypes from "../../store/actions";
import Aux from "../../hoc/_Aux";

import { Redirect } from 'react-router'

class CreateContrat extends Component {
    constructor(props){
        super(props)
        
        // mise en place des fonctions
        this.handleChange = this.handleChange.bind(this)

        // mise en place du state
        this.state = {
            entreprises: [],
            ports: [],
            entreprise: '',
            port: '',
            refPolice: '',
            refCompta: '',
            remarque: '',
            foisDeux: false,
            success: false
        }
    }
 
    componentDidMount(){
        if (this.props.loggedIn){
                  
           apiClient.get('/api/entreprises')
                   
           .then(response => {
               let listeEntr = []
               let nomsEntreprises = []
               listeEntr = response.data
               //console.log(listeEntr)
               listeEntr.map((entreprise) => {
                   nomsEntreprises.push(entreprise.id + '  ' + entreprise.nom)
               })
               this.setState({ entreprises: nomsEntreprises })
               //console.log(this.state.entreprises)
           })
           .catch(error => console.log(error))
           
           
           
           apiClient.get('/api/indexBE')
                   
           .then(response => {
               // console.log(response.data)
               let listePorts = []
               let nomsPorts = []
               listePorts = response.data
              
               listePorts = Object.values(listePorts)
               
               listePorts.map((port) => {
                   nomsPorts.push(port.id + '    ' + port.nomFr)
               }) 

               this.setState({ ports: nomsPorts })
               //console.log(this.state.ports)
           })
           .catch(error => console.log(error))
           }
       }
    
      

    handleChange = (e) => {
        //console.log(e);
        
        this.setState({
            // on va chercher l'id de l'élément sur lequel il y a un chgt, on prend la valeur et on va la mettre dans le state
            [e.target.name]: e.target.value
        }) 
        
    }

    
    handleSubmit = (e) => {
        e.preventDefault()
        let entreprise = this.state.entreprise.split(' ')
        //console.log ('l 92 ' + entreprise[0])
        let port = this.state.port.split(' ')
        this.state.foisDeux === false ? this.state.foisDeux = 0 : this.state.foisDeux = 1
        const contrat = {
            refPolice: this.state.refPolice,
            refCompta: this.state.refCompta,
            remarque: this.state.remarque,
            foisDeux: this.state.foisDeux,
            created_at: new Date(),
            updated_at: new Date(),
            ville_id : port[0],
            entreprise_id : entreprise[0],
            
        }
        if (this.props.loggedIn){
            //console.log(contrat)
            apiClient.post(`/api/contrats`, contrat)
                .then(response => {
                    console.log(response.data)
                    this.setState({ success: true })
                    /* reset du formulaire */
                    Object.keys(contrat).forEach(item => {
                            contrat[item] = ''
                    }) 
                    Swal.fire(
                        'Le contrat a bien été encodé',
                    )
                }).catch(error =>{
                    console.log(error)
                })
                    
                
                
                
                   
            }
         
        }
    

    render(){
        const entreprises = this.state.entreprises.map((entreprise, index) => {
            return <option name='entreprise' key={ index }>{ entreprise }</option>
        })
        const ports = this.state.ports.map((port, index) => {
            return <option name='port' key={ index }>{ port }</option>
        })
        if (this.props.loggedIn && !this.state.success){
            return (
                <Aux>
                    <Row>
                        <Col>
                            <Card>
                                <Card.Header>
                                    <Card.Title as="h5">Ajout d'un contrat</Card.Title>
                                </Card.Header>
                                <Card.Body>
                                
                                    <Row>
                                        <Col md={6}>
                                            <Form onSubmit={ this.handleSubmit }>
                                                <Form.Group controlId="Nom">
                                                    <Form.Label>Entreprise</Form.Label>
                                                    <Form.Control as="select" className="mb-3" name='entreprise' value={this.state.entreprise} onChange={ this.handleChange }>
                                                        <option>Entreprise</option>
                                                        { entreprises }         
                                                    </Form.Control>
                                                    <Form.Label>Port</Form.Label>
                                                    <Form.Control as="select" className="mb-3" name='port' value={this.state.port} onChange={ this.handleChange }>
                                                        <option>Port</option>
                                                        { ports }         
                                                    </Form.Control>
                                                    <Form.Label>Référence police</Form.Label>
                                                    <Form.Control type="text" name='refPolice' value={ this.state.refPolice } onChange= { this.handleChange } />
                                                    <Form.Label>Référence compta</Form.Label>
                                                    <Form.Control type="text" name='refCompta' value={ this.state.refCompta } onChange= { this.handleChange } />
                                                    <Form.Label>Remarque</Form.Label>
                                                    <Form.Control type="text" name='remarque' value={ this.state.remarque } onChange= { this.handleChange } />
                                                    <Form.Label>Fois deux</Form.Label>
                                                    <Form.Control type="text" name='foisDeux' value={ this.state.foisDeux } onChange= { this.handleChange } />
                                                </Form.Group>
    
                                                <Button type="submit" variant={'outline-primary'}><UcFirst text='créer un contrat' /></Button>
                                                    
                                            
                                            </Form>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    
                </Aux>
            )
    
        }
        else if (this.props.loggedIn && this.state.success) {
            return <Redirect to='/Contrats'/>
        }
        else {
            return <h4>Veuillez vous connecter</h4>
        }
        
    }
}

const mapStateToProps = state => {
    return {
        loggedIn: state.bases.loggedIn
    }
};

export default connect(mapStateToProps)(CreateContrat);