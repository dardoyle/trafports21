import React, {useEffect} from 'react';
import {NavLink, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2'

import '../../assets/scss/style.scss';
import Aux from "../../hoc/_Aux";
import Breadcrumb from "../../App/layout/AdminLayout/Breadcrumb";

import { connect } from 'react-redux'
import * as actionTypes from '../../store/actions'
import apiClient from '../../services/api'
import { apiDatasCall } from '../../store/datas/actionData'
import Cookies from 'js-cookie'
import Dashboard from '../../Demo/Dashboard/Default'
import SignIn1 from '../../Demo/Authentication/SignIn/SignIn1'

import {Form, FormControl} from 'react-bootstrap';


const CreateUser = ({roles}) => {


    const [name, setName] = React.useState('')
    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [confirm_password, setConfirmPassword] = React.useState('')
    const [message, setMessage] = React.useState('')
    const [role, setRole] = React.useState([])
    const [success, setSuccess] = React.useState(false)
      
          
    //console.log(roles)
    function RolesList(roles){
        let rolesList = []
        if(roles != undefined){
            rolesList = roles.map((role) =>
            <option name='role' key={role.id}>{role.name}</option>)
        }
        
        return rolesList
    } 
    
    const liste = RolesList(roles)
    //setListeRoles(liste)


    const handleChange = (e) => {
       
        setRole(e.target.value) 
        
    }
    
    const handleSubmit = (e) => {
        e.preventDefault()
        apiClient.get('sanctum/csrf-cookie')
            .then(response => {
                apiClient.post('api/register',{
                    name: name,
                    email: email,
                    password: password,
                    confirm_password: confirm_password,
                    role: role
                }).then(response => {
                    if(response.data.error){
                        console.log(response.data.error)
                    }
                    else{
                        setSuccess(true)
                        Swal.fire(
                            'Le nouvel utilisateur a bien été enregistré',
                        )                
                    }
            
            }).catch(error => {
                console.log(error)
                setMessage('Les deux mots de passe doivent être identiques')
        })
        });
    }
    
    if(message === 'login'){
        
        return(<Redirect to="/auth/signin-1" component={SignIn1}/>)
    } 
    else if (success === true){

        return <Redirect to='/users'/>
    }
    else {
        return(
            <Aux>
                <Breadcrumb/>
                <div className="auth-wrapper">
                    <div className="auth-content">
                        <div className="auth-bg">
                            <span className="r"/>
                            <span className="r s"/>
                            <span className="r s"/>
                            <span className="r"/>
                        </div>
                        <div className="card">
                            <div className="card-body text-center">
                                <div className="mb-4">
                                    <i className="feather icon-user-plus auth-icon"/>
                                </div>
                                <h3 className="mb-4">Nouvel enregistrement</h3>
                                <form onSubmit = {handleSubmit}>
                                    <div className="input-group mb-3">
                                        <input 
                                            type="text" 
                                            className="form-control" 
                                            placeholder="username" 
                                            name="name" 
                                            value={name}
                                            onChange={e => setName(e.target.value)}
                                        />
                                    </div>
                                    <div className="input-group mb-3">
                                        <input 
                                            type="email" 
                                            className="form-control" 
                                            placeholder="email"
                                            name="email"
                                            value={email}
                                            onChange={e => setEmail(e.target.value)}
                                        />
                                    </div>
                                    <div className="input-group mb-4">
                                        <input 
                                            type="password" 
                                            className="form-control" 
                                            placeholder="password"
                                            name="password"
                                            value={password}
                                            onChange={e => setPassword(e.target.value)}
                                        />
                                    </div>
                                    <div className="input-group mb-4">
                                        <input 
                                            type="password" 
                                            className="form-control" 
                                            placeholder="confirm password"
                                            name="confirm_password"
                                            value={confirm_password}
                                            onChange={e => setConfirmPassword(e.target.value)}
                                        />
                                    </div>
                                    <Form.Control as="select" className="mb-3" name='role' value={role} onChange={ handleChange }>
                                        <option>select a role</option>
                                        { liste }         
                                    </Form.Control>
                                                                       
                                    <button className="btn btn-primary shadow-2 mb-4">Créer le compte</button>
                                </form>
                                {message ? <div class="alert alert-warning" role="alert">{message}</div> : ''}
                                
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
    );
        };
    
}

const mapStateToProps = state => {
    return {
        role: state.bases.role,
        loggedIn: state.bases.loggedIn,
        userName: state.bases.userName,
        roles: state.datas.roles
    }
}

const mapDispatchToProps = dispatch => {
    return {
        applyRole: (role) => dispatch({type: actionTypes.APPLY_ROLE, role}),
        setLogin: (loggedIn) => dispatch({type: actionTypes.LOGIN, loggedIn}),
        setUserName: (userName) => dispatch({type: actionTypes.SET_USERNAME, userName}),
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateUser);