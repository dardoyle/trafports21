import React, { Component } from 'react'
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';
import UcFirst from "../../App/components/UcFirst"
import Swal from 'sweetalert2'

import apiClient from '../../services/api';
import { connect } from 'react-redux'
import * as actionTypes from "../../store/actions";
import Aux from "../../hoc/_Aux";

import { Redirect } from 'react-router'

class CreateEntreprise extends Component {
    constructor(props){
        super(props)
        
        // mise en place des fonctions
        this.handleChange = this.handleChange.bind(this)

        // mise en place du state
        this.state = {
            nom: '',
            adresse: '',
            tel: '',
            mail: '',
            ports: [],
            port: '',
            success: false
        }
    }

    componentDidMount(){
        if (this.props.loggedIn){
            apiClient.get('/api/indexBE')
                   
           .then(response => {
               // console.log(response.data)
               let listePorts = []
               let nomsPorts = []
               listePorts = response.data
              
               listePorts = Object.values(listePorts)
               
               listePorts.map((port) => {
                   nomsPorts.push(port.id + '    ' + port.nomFr)
               }) 

               this.setState({ ports: nomsPorts })
               //console.log(this.state.ports)
           })
           .catch(error => console.log(error))
           }
    }
 
       

    handleChange = (e) => {
        //console.log(e);
        
        this.setState({
            // on va chercher l'id de l'élément sur lequel il y a un chgt, on prend la valeur et on va la mettre dans le state
            [e.target.name]: e.target.value
        }) 
        
    }

        
    handleSubmit = (e) => {
        e.preventDefault()
        let port = this.state.port.split(' ')
        const entreprise = {
            nom: this.state.nom,
            adresse: this.state.adresse,
            tel: this.state.tel,
            mail: this.state.mail,
            created_at: new Date(),
            updated_at: new Date(),
            cp_id: port[0]
            
        }
        if (this.props.loggedIn){
            //console.log(contrat)
            apiClient.post(`/api/entreprises`, entreprise)
                .then(response => console.log(response.data))
                Swal.fire(
                    'L\'entreprise a bien été encodée',
                )
                /* reset du formulaire */
               /*  Object.keys(contrat).forEach(item => {
                    contrat[item] = ''
                }) */
                this.setState({ success: true })
                .catch(error => {
                    console.log(error)
                    //setMessage('Les deux mots de passe doivent être identiques')
            })
                
         
        }
        
    }
    

    render(){
        const ports = this.state.ports.map((port, index) => {
            return <option name='port' key={ index }>{ port }</option>
        })
        if (this.props.loggedIn && !this.state.success){
            return (
                <Aux>
                    <Row>
                        <Col>
                            <Card>
                                <Card.Header>
                                    <Card.Title as="h5">Ajout d'une entreprise</Card.Title>
                                </Card.Header>
                                <Card.Body>
                                
                                    <Row>
                                        <Col md={6}>
                                            <Form onSubmit={ this.handleSubmit }>
                                                <Form.Group controlId="Nom">
                                                    <Form.Label>Nom</Form.Label>
                                                    <Form.Control type="text" name='nom' value={ this.state.nom } onChange= { this.handleChange } />
                                                    <Form.Label>Adresse</Form.Label>
                                                    <Form.Control type="text" name='adresse' value={ this.state.adresse } onChange= { this.handleChange } />
                                                    <Form.Label>Ville / Port</Form.Label>
                                                    <Form.Control as="select" className="mb-3" name='port' value={this.state.port} onChange={ this.handleChange }>
                                                        <option>Sélectionnez un port</option>
                                                        { ports }         
                                                    </Form.Control>
                                                    <Form.Label>Téléphone</Form.Label>
                                                    <Form.Control type="text" name='tel' value={ this.state.tel } onChange= { this.handleChange } />
                                                    <Form.Label>Email</Form.Label>
                                                    <Form.Control type="text" name='mail' value={ this.state.mail } onChange= { this.handleChange } />
                                                </Form.Group>
    
                                                <Button type="submit" variant={'outline-primary'}><UcFirst text='créer une entreprise' /></Button>
                                                    
                                            
                                            </Form>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    
                </Aux>
            )
    
        }
        else if (this.props.loggedIn && this.state.success) {
            return <Redirect to='/Entreprises'/>
        }
        else {
            return <h4>Veuillez vous connecter</h4>
        }
        
    }
}

const mapStateToProps = state => {
    return {
        loggedIn: state.bases.loggedIn
    }
};

export default connect(mapStateToProps)(CreateEntreprise);