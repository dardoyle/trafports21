import React, { Component } from 'react'
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';
import UcFirst from "../../App/components/UcFirst"
import axios from 'axios'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'



import apiClient from '../../services/api';
import { connect } from 'react-redux'
import * as actionTypes from "../../store/actions";
import Aux from "../../hoc/_Aux";

import { Redirect } from 'react-router'

class CreateZone extends Component {
    constructor(props){
        super(props)
        
        // mise en place des fonctions
        this.handleChange = this.handleChange.bind(this)

        // mise en place du state
        this.state = {
            nom: '',
            success: false
        }
    }
    
      

    handleChange = (e) => {
        //console.log(e);
        
        this.setState({
            // on va chercher l'id de l'élément sur lequel il y a un chgt, on prend la valeur et on va la mettre dans le state
            [e.target.name]: e.target.value
        }) 
        
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const zone = {
            nom : this.state.nom,
        }
        if (this.props.loggedIn){
            apiClient.post(`/api/zones`, zone)
                .then(response => {
                    console.log(response.data)
                    Swal.fire({
                        title: 'Succès',
                        text: 'La zone a bien été encodée',
                        icon: 'success'
                      }
                    )
                    /* reset du formulaire */
                    Object.keys(zone).forEach(item => {
                        zone[item] = ''
                    })
                    this.setState({ success: true })

                })
                .catch(error => {
                    console.log(error)
                    Swal.fire({
                            title: 'Erreur !',
                            text: 'La zone existe déjà dans la base de données',
                            icon: 'error'
                        }
                        
                    )}
                    )
                
                
         
        }
    }

    


    render(){
        if (this.props.loggedIn && !this.state.success){
            return (
                <Aux>
                    <Row>
                        <Col>
                            <Card>
                                <Card.Header>
                                    <Card.Title as="h5">Ajout d'une zone</Card.Title>
                                </Card.Header>
                                <Card.Body>
                                
                                    <Row>
                                        <Col md={6}>
                                            <Form onSubmit={ this.handleSubmit }>
                                                <Form.Group controlId="Nom">
                                                    <Form.Label>Nom</Form.Label>
                                                    <Form.Control type="text" name='nom' value={ this.state.nom } onChange= { this.handleChange } />
                                                </Form.Group>
    
                                                <Button type="submit" variant={'outline-primary'}><UcFirst text='créer une zone' /></Button>
                                                    
                                            
                                            </Form>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    
                </Aux>
            )
    
        }
        else if (this.props.loggedIn && this.state.success) {
            return <Redirect to='/Zones'/>
        }
        else {
            return <h4>Veuillez vous connecter</h4>
        }
        
    }
}

const mapStateToProps = state => {
    return {
        loggedIn: state.bases.loggedIn
    }
};

export default connect(mapStateToProps)(CreateZone);