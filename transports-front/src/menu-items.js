export default {
    items: [
        {
            id: 'navigation',
            title: '',
            type: 'group',
            icon: 'icon-navigation',
            level: 1,
            children: [
                {
                    id: 'dashboard',
                    title: 'Accueil',
                    type: 'item',
                    url: '/dashboard/default',
                    icon: 'feather icon-home',
                    level: 1,
                }
            ]
        },
        
        {
            id: 'pages',
            title: 'gestion',
            type: 'group',
            icon: 'icon-pages',
            level: 1,
            children: [
                {
                    id: 'auth',
                    title: 'Utilisateurs',
                    type: 'collapse',
                    url: '/users',
                    icon: 'feather icon-lock',
                    level: 4,
                    children: [
                        {
                            id: 'users',
                            title: 'Liste',
                            type: 'item',
                            url: '/users',
                            breadcrumbs: false,
                            level: 4
                        },
                        {
                            id: 'ajoutUser',
                            title: 'Ajouter un utilisateur',
                            type: 'item',
                            url: '/ajouter-user',
                            breadcrumbs: false,
                            level: 4
                        },
                    ]
                },
                {
                    id: 'contrats',
                    title: 'Contrats',
                    type: 'collapse',
                    url: '/contrats',
                    icon: 'feather icon-sidebar',
                    level: 4,
                    children : [
                        {
                            id: 'listeContrats',
                            title: 'Liste',
                            type: 'item',
                            url: '/contrats',
                            classes: 'nav-item',
                            icon: 'feather icon-sidebar',
                            level: 4
                        },
                        {
                            id: 'ajoutContrat',
                            title: 'Ajouter',
                            type: 'item',
                            url: '/ajouter-contrat',
                            classes: 'nav-item',
                            icon: 'feather icon-sidebar',
                            level: 4
                        },            
                    ] 
                },

                               
                {
                    id: 'entreprises',
                    title: 'Entreprises',
                    type: 'collapse',
                    url: '/entreprises',
                    icon: 'feather icon-sidebar',
                    level: 4,
                    children : [
                        {
                            id: 'listeEntreprises',
                            title: 'Liste',
                            type: 'item',
                            url: '/entreprises',
                            classes: 'nav-item',
                            icon: 'feather icon-sidebar',
                            level: 4
                        },
                        {
                            id: 'ajoutEnteprise',
                            title: 'Ajouter',
                            type: 'item',
                            url: '/ajouter-entreprise',
                            classes: 'nav-item',
                            icon: 'feather icon-sidebar',
                            level: 4
                        },            
                    ] 
                },
                {
                    id: 'marchandises',
                    title: 'Marchandises',
                    type: 'item',
                    url: '/marchandises',
                    classes: 'nav-item',
                    icon: 'feather icon-sidebar',
                    level: 4
                },
                {
                    id: 'NST',
                    title: 'NST',
                    type: 'item',
                    url: '/nst',
                    classes: 'nav-item',
                    icon: 'feather icon-sidebar',
                    level: 4
                },
                {
                    id: 'ports',
                    title: 'Ports',
                    type: 'item',
                    url: '/ports',
                    classes: 'nav-item',
                    icon: 'feather icon-sidebar',
                    level: 4
                },
                {
                    id: 'releves',
                    title: 'Releves',
                    type: 'item',
                    url: '/releves',
                    classes: 'nav-item',
                    icon: 'feather icon-sidebar',
                    level: 1
                },
                {
                    id: 'zones',
                    title: 'Zones',
                    type: 'collapse',
                    url: '/zones',
                    icon: 'feather icon-sidebar',
                    level: 4,
                    children : [
                        {
                            id: 'listeZones',
                            title: 'Liste',
                            type: 'item',
                            url: '/zones',
                            classes: 'nav-item',
                            icon: 'feather icon-sidebar',
                            level: 4
                        },
                        {
                            id: 'ajoutZone',
                            title: 'Ajouter',
                            type: 'item',
                            url: '/ajouter-zone',
                            classes: 'nav-item',
                            icon: 'feather icon-sidebar',
                            level: 4
                        },            
                    ] 
                },
                               
                
                /* {
                    id: 'docs',
                    title: 'Documentation',
                    type: 'item',
                    url: '/docs',
                    classes: 'nav-item',
                    icon: 'feather icon-help-circle'
                }, */
                {
                    id: 'menu-level',
                    title: 'Menu Levels',
                    type: 'collapse',
                    icon: 'feather icon-menu',
                    children: [
                        {
                            id: 'menu-level-1.1',
                            title: 'Menu Level 1.1',
                            type: 'item',
                            url: '#!',
                        },
                        {
                            id: 'menu-level-1.2',
                            title: 'Menu Level 2.2',
                            type: 'collapse',
                            children: [
                                {
                                    id: 'menu-level-2.1',
                                    title: 'Menu Level 2.1',
                                    type: 'item',
                                    url: '#',
                                },
                                {
                                    id: 'menu-level-2.2',
                                    title: 'Menu Level 2.2',
                                    type: 'collapse',
                                    children: [
                                        {
                                            id: 'menu-level-3.1',
                                            title: 'Menu Level 3.1',
                                            type: 'item',
                                            url: '#',
                                        },
                                        {
                                            id: 'menu-level-3.2',
                                            title: 'Menu Level 3.2',
                                            type: 'item',
                                            url: '#',
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }, 
                
            ]
        }
    ]
}