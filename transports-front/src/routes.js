import React from 'react';
import $ from 'jquery';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

//const Login = React.lazy(() => import('./Demo/Authentication/SignIn/SignIn1'));

const DashboardDefault = React.lazy(() => import('./Demo/Dashboard/Default'));

const UIBasicButton = React.lazy(() => import('./Demo/UIElements/Basic/Button'));
const UIBasicBadges = React.lazy(() => import('./Demo/UIElements/Basic/Badges'));
const UIBasicBreadcrumbPagination = React.lazy(() => import('./Demo/UIElements/Basic/BreadcrumbPagination'));

const UIBasicCollapse = React.lazy(() => import('./Demo/UIElements/Basic/Collapse'));
const UIBasicTabsPills = React.lazy(() => import('./Demo/UIElements/Basic/TabsPills'));
const UIBasicBasicTypography = React.lazy(() => import('./Demo/UIElements/Basic/Typography'));

const FormsElements = React.lazy(() => import('./Demo/Forms/FormsElements'));

const BootstrapTable = React.lazy(() => import('./Demo/Tables/BootstrapTable'));

const Nvd3Chart = React.lazy(() => import('./Demo/Charts/Nvd3Chart/index'));

const GoogleMap = React.lazy(() => import('./Demo/Maps/GoogleMap/index'));

const OtherSamplePage = React.lazy(() => import('./Demo/Other/SamplePage'));
const OtherDocs = React.lazy(() => import('./Demo/Other/Docs'));

const Contrats = React.lazy(() => import('./Transports/Listes/Contrats'));
const Contrat = React.lazy(() => import('./Transports/Details/Contrat'));
const CreateContrat = React.lazy(() => import('./Transports/Creations/CreateContrat'));
const EditContrat = React.lazy(() => import('./Transports/Editions/EditContrat'));
const Entreprises = React.lazy(() => import('./Transports/Listes/Entreprises'));
const Entreprise = React.lazy(() => import('./Transports/Details/Entreprise'));
const CreateEntreprise = React.lazy(() => import('./Transports/Creations/CreateEntreprise'));
const Marchandises = React.lazy(() => import('./Transports/Listes/Marchandises'));
const Nst = React.lazy(() => import('./Transports/Listes/NST'));
const Pays = React.lazy(() => import('./Transports/Listes/Pays'));
const Ports = React.lazy(() => import('./Transports/Listes/Ports'));
const Releves = React.lazy(() => import('./Transports/Listes/Releves'));
const Statuts = React.lazy(() => import('./Transports/Listes/Statuts'));
const Transports = React.lazy(() => import('./Transports/Listes/Transports'));
const Users = React.lazy(() => import('./Transports/Listes/Users'));
const User = React.lazy(() => import('./Transports/Details/User'));
const CreateUser = React.lazy(() => import('./Transports/Creations/CreateUser'));
const Zones = React.lazy(() => import('./Transports/Listes/Zones'));
const CreateZone = React.lazy(() => import('./Transports/Creations/CreateZone'));
const Datas = React.lazy(() => import('./Transports/Listes/DatasSliced'));

const routes = [
    /* { path: '/login', exact: true, name: 'Default', component: Login }, */
    { path: '/dashboard/default', exact: true, name: 'Default', component: DashboardDefault },
    { path: '/basic/button', exact: true, name: 'Basic Button', component: UIBasicButton },
    { path: '/basic/badges', exact: true, name: 'Basic Badges', component: UIBasicBadges },
    { path: '/basic/breadcrumb-paging', exact: true, name: 'Basic Breadcrumb Pagination', component: UIBasicBreadcrumbPagination },
    { path: '/basic/collapse', exact: true, name: 'Basic Collapse', component: UIBasicCollapse },
    { path: '/basic/tabs-pills', exact: true, name: 'Basic Tabs & Pills', component: UIBasicTabsPills },
    { path: '/basic/typography', exact: true, name: 'Basic Typography', component: UIBasicBasicTypography },
    { path: '/forms/form-basic', exact: true, name: 'Forms Elements', component: FormsElements },
    { path: '/tables/bootstrap', exact: true, name: 'Bootstrap Table', component: BootstrapTable },
    { path: '/charts/nvd3', exact: true, name: 'Nvd3 Chart', component: Nvd3Chart },
    { path: '/maps/google-map', exact: true, name: 'Google Map', component: GoogleMap },
    { path: '/sample-page', exact: true, name: 'Sample Page', component: OtherSamplePage },
    { path: '/docs', exact: true, name: 'Documentation', component: OtherDocs },
    { path: '/contrats', exact: true, name: 'Contrats', component: Contrats },
    { path: '/contrats/:slug', exact: false, name: 'DetailsContrat', component: Contrat },
    { path: '/ajouter-contrat', exact: true, name: 'CreateContrat', component: CreateContrat },
    { path: '/editer-contrats/:id', exact: true, name: 'EditContrat', component: EditContrat },
    { path: '/entreprises', exact: true, name: 'Entreprises', component: Entreprises },
    { path: '/entreprises/:slug', exact: false, name: 'DetailsEntreprise', component: Entreprise },
    { path: '/ajouter-entreprise', exact: true, name: 'CreateContrat', component: CreateEntreprise },
    { path: '/marchandises', exact: true, name: 'Marchandises', component: Marchandises },
    { path: '/nst', exact: true, name: 'Nst', component: Nst },
    { path: '/pays', exact: true, name: 'Pays', component: Pays },
    { path: '/ports', exact: true, name: 'Ports', component: Ports },
    { path: '/releves', exact: true, name: 'Releves', component: Releves },
    { path: '/statuts', exact: true, name: 'Statuts', component: Statuts },
    { path: '/users', exact: true, name: 'Users', component: Users },
    { path: '/users/:slug', exact: false, name: 'DetailsUser', component: User },
    { path: '/ajouter-user', exact: true, name: 'CreateUser', component: CreateUser },
    { path: '/zones', exact: true, name: 'Zones', component: Zones },
    { path: '/ajouter-zone', exact: true, name: 'CreateZone', component: CreateZone },
       
];

export default routes;