import React from 'react';
import Spinner from 'react-loader-spinner'

const loader = () => {
    return (
        <div className="loader-bg">
            <div className="loader-track">
                <div className="loader-fill"/>
            </div>
            <div className="loader-spin">
                <Spinner type='BallTriangle' color='#3f4d67' height={100} width={100} />
            </div>
        </div>
    );
};

export default loader;




