import React from 'react';
import Aux from "../../../../../../hoc/_Aux";
import NavCollapse from './../NavCollapse';
import NavItem from './../NavItem';
import {connect} from 'react-redux';

const navGroup = (props) => {
    //console.log ('NavGroup ' + props.role)
    let navItems = '';
    if (props.group.children) {
        const groups = props.group.children;
          
        navItems = Object.keys(groups).map(item => {
            
            item = groups[item];
            
            switch (props.role){
                
                case 'concess' : {
                    if (item.level <= 1){
                        switch (item.type) {
                            case 'collapse':
                                return <NavCollapse key={item.id} collapse={item} type="main" />;
                            case 'item':
                                return <NavItem layout={props.layout} key={item.id} item={item} />;
                            default:
                                return false;
                        }            
                    }
                    break
                }
                case 'police' : {
                    if (item.level <= 2){
                        //menuSwitch(item)
                        switch (item.type) {
                            case 'collapse':
                                return <NavCollapse key={item.id} collapse={item} type="main" />;
                            case 'item':
                                return <NavItem layout={props.layout} key={item.id} item={item} />;
                            default:
                                return false;
                        }            
                    }
                    break
                }
                case 'compta' : {
                    if (item.level <= 3){
                        //menuSwitch(item)
                        switch (item.type) {
                            case 'collapse':
                                return <NavCollapse key={item.id} collapse={item} type="main" />;
                            case 'item':
                                return <NavItem layout={props.layout} key={item.id} item={item} />;
                            default:
                                return false;
                        }            
                    }
                    break
                }
                case 'admin' : {
                    if (item.level <= 4){
                        //menuSwitch(item)
                        switch (item.type) {
                            case 'collapse':
                                return <NavCollapse key={item.id} collapse={item} type="main" />;
                            case 'item':
                                return <NavItem layout={props.layout} key={item.id} item={item} />;
                            default:
                                return false;
                        }            
                    }
                    break
                }
                default : {
                    return item.level
                }
            }   
            
        });
    }

    return (
        <Aux>
            <li key={props.group.id} className="nav-item pcoded-menu-caption"><label>{props.group.title}</label></li>
            {navItems}
        </Aux>
    );
};

const mapStateToProps = state => {
    return {
        role: state.bases.role
    }
};

export default connect(mapStateToProps, null)(navGroup);