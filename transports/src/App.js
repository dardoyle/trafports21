import logo from './logo.svg';
import './App.css';
import React from 'react';
import { BrowserRouter as Router, Switch, Route, NavLink } from 'react-router-dom';
import Login from './components/Login';
import Home from './components/Home'
import Dashboard from './components/Dashboard/Default';


const App = () => {
  return (
      <Router>
          <Switch>
              <Route exact path='/' component={Login} />
              <Route path='/dashboard' component={Dashboard} />
          </Switch>
      </Router>
  );
};

export default App;
