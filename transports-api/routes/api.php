<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Zones;
use App\Http\Controllers\Contrats;
use App\Http\Controllers\CPs;
use App\Http\Controllers\Entreprises;
use App\Http\Controllers\Marchandises;
use App\Http\Controllers\Nsts;
use App\Http\Controllers\PaysController;
use App\Http\Controllers\Ports;
use App\Http\Controllers\Releves;
use App\Http\Controllers\Users;
use App\Http\Controllers\Statuts;
use App\Http\Controllers\Transports;
use App\Http\Controllers\TypesPorts;

use App\Http\Controllers\Permissions;
use App\Http\Controllers\Roles;

use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentification
Route::post('login', [AuthController::class, 'signin']);
Route::post('register', [AuthController::class, 'signup']);



// create a resource
// 1 - create the db and migrations
// 2 - create a model
// 2.5 - create a service ? (eloquent ORM)
// 3 - create a CTRL to get info from the db
// 4 - return the info

// zones
// 1 - get all (GET) /api/zones

// 2 - create a new one (POST) /api/zones

// 3 - get as single one (GET) /api/zones/{id}

// 4 - update a single (PUT/PATCH) /api/zones/{id}

// 5 - delete (DELETE) /api/zones/{id}

Route::middleware('auth:sanctum')->group( function() {
    Route::get('user', [AuthController::class, 'user']);
    Route::apiResources([
        'contrats' => Contrats::class,
        'CPs' => CPs::class,
        'entreprises' => Entreprises::class,
        'marchandises' => Marchandises::class,
        'nsts' => Nsts::class,
        'pays' => PaysController::class,
        'ports' => Ports::class,
        'releves' => Releves::class,
        'statuts' => Statuts::class,
        'transports' => Transports::class,
        'typesPorts' => TypesPorts::class,
        'users' => Users::class,
        'zones' => Zones::class,
    ]);
    Route::put('users/info', [AuthController::class, 'updateInfo']);
    Route::put('users/password', [AuthController::class, 'updatePassword']);
    Route::post('loadMoreContrats', [Contrats::class, 'loadMoreContrats'])->name('loadMoreContrats');
    Route::get('indexBE', [Ports::class, 'indexBE'])->name('portsBE');
    Route::get('permissions', [Permissions::class, 'index']);
    Route::get('roles', [Roles::class, 'index']);
    Route::post('logout', [AuthController::class, 'logout']);
});





