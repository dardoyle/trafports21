<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Base as BaseController;
use Validator;
use App\Models\Zone;
use App\Http\Resources\Zone as ZoneResource;


class Zones extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // list of all zones
        return Zone::all();
        //return $this->sendResponse(ZoneResource::collection($zones), 'Zones rappatriées.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data first
        $input = $request->all();   
        $validator = Validator::make($input, [
            'nom' => 'required|unique:zones',
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());
        }
        
        // add a new zone
        $zone = Zone::create($request->all());
        return $this->sendResponse(new ZoneResource($zone), 'Zone créée');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // details of one zone
        $zone = Zone::find($id);
        if(is_null($zone)){
            return $this->sendError('Zone inexistante');
        }
        return $this->sendResponse(new ZoneResource($zone), 'Zone rappatriée');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate data first
        $input = $request->all();   
        $validator = Validator::make($input, [
            'nom' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());
        }

        // update one zone
        $zone = Zone::find($id);
        $zone->update($request->all());
        
        return $this->sendResponse(new ZoneResource($zone), 'Zone mise à jour');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Zone $zone)
    {
        // delete one zone
        $zone->delete();
        return $this->sendResponse([], 'Zone supprimée');
    }
}
