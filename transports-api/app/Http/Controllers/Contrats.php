<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contrat;
use App\Models\Entreprise;
use App\Models\Port;

class Contrats extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

// get all contrats
        return Contrat::with(['entreprise:id,nom', 'port:id,nomFr'])->get();
    }

    /**
     * Load more data
     */
    public function loadMoreContrats(Request $request){


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data first
        $request->validate([
          'refPolice' => 'required|unique:contrats',
          'refCompta' => 'required|unique:contrats',
          'ville_id' => 'required',
          'entreprise_id' => 'required'  
        ]);

        // create a contrat
        //return Contrat::create($request->all());
        $data = $request->all();
      
        $contrat = new Contrat;
        $contrat->refPolice = $data['refPolice'];
        $contrat->refCompta = $data['refCompta'];
        $contrat->remarque = $data['remarque'];
        $contrat->foisDeux = $data['foisDeux'];
        $contrat->ville_id = $data['ville_id'];
        $contrat->entreprise_id = $data['entreprise_id'];
        $contrat->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // show a contrat
        return Contrat::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // update a contrat
        $contrat = Contrat::find($id);
        $data = $request->all();
      
        $contrat->refPolice = $data['refPolice'];
        $contrat->refCompta = $data['refCompta'];
        $contrat->remarque = $data['remarque'];
        $contrat->foisDeux = $data['foisDeux'];
        $contrat->ville_id = $data['ville_id'];
        $contrat->entreprise_id = $data['entreprise_id'];
        $contrat->save();
        /* $contrat->update($request->all());
        return $contrat; */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete a contract
        return Contrat::destroy($id);
    }
}
