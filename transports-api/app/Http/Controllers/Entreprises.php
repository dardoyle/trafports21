<?php

namespace App\Http\Controllers;

use App\Models\Entreprise;
use Illuminate\Http\Request;

class Entreprises extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // list of entreprises
        return Entreprise::with(['port:id,nomFr'])->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data first
        $request->validate([
            'nom' => 'required|unique:entreprises',
            'adresse' => 'required',
            'tel' => 'nullable',
            'mail' => 'nullable',
            'cp_id' => 'required',
                      
          ]);

        // create an enterprise
        
        $data = $request->all();
      
        $entreprise = new Entreprise;
        $entreprise->nom = $data['nom'];
        $entreprise->adresse = $data['adresse'];
        $entreprise->cp_id = $data['cp_id'];
        $entreprise->tel = $data['tel'];
        $entreprise->mail = $data['mail'];
        $entreprise->save();
    


    }

    
        


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // details of one entreprise
        return Entreprise::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // update one entreprise
        $entr = Entreprise::find($id);
        $entr->update($request->all());
        return $entr;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete one entreprise
        return Entreprise::destroy($id);
    }
}
