<?php

namespace App\Http\Controllers;

use App\Models\Marchandise;
use Illuminate\Http\Request;

class Marchandises extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // list of marchandises
        return Marchandise::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data first
        $request->validate([
            'nom' => 'required',
              
          ]);

        // create a new marchandise
        return Marchandise::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // details of one marchandise
        return Marchandise::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // update one marchandise
        $marchandise = Marchandise::find($id);
        $marchandise->update($request->all());
        return $marchandise;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete one marchandise
        return Marchandise::destroy($id);
    }
}
