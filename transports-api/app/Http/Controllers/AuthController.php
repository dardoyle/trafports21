<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Base as BaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\User;
   
class AuthController extends BaseController
{

    public function signin(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
             
            $authUser = Auth::user(); 
            $success['token'] =  $authUser->createToken('MyAuthApp')->plainTextToken; 
            $success['name'] =  $authUser->name;
            $success['role'] = $authUser->getRoleNames();

            

            return $this->sendResponse($success, 'User signed in');
        } 
        else{ 
            
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        } 
    }

    public function logout(Request $request){
        $user = User::where('email', $request->email)->first();
        $user->tokens()->delete();
        return ['message' => 'token deleted'];
    }

    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Error validation', $validator->errors());       
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        if(isset($input['role'])){
            $user->assignRole($input['role']);
        }
        else{
            $user->assignRole('admin');
        }
        

        $success['token'] =  $user->createToken('MyAuthApp')->plainTextToken;
        $success['name'] =  $user->name;
   
        return $this->sendResponse($success, 'User created successfully.');
    }

    public function user(Request $request)
    {
        return $request->user();
    }

    /**
     * logged user who updates his own datas
     * @param $user
     * @return $user updated
     */
    public function updateInfo(UpdateInfoRequest $request)
    {
        $user =$request->user();
        $user->update($request->only(['name', 'email']));
        return response($user, Response::HTTP_ACCEPTED);
    }

    /**
     * loggued user who updates his own datas
     * @param $user
     * @return $user updated
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user =$request->user();
        $user->update([
            'password' => bcrypt($request->input(['password']))
        ]);
        return response($user, Response::HTTP_ACCEPTED);
    }
   
}