<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class Users extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // list all ports
        //return User::paginate(5);
        return User::All();
    }

    /**
     * Creates a user and gives him a pwd 1234
     */
     /* public function store(Request $request)
    {
        $user = User::create(
            $request->only(['name', 'email']) + ['password' => bcrypt(value: 1234)])
        );

        return response($user, Response::HTTP_CREATED)
    } */ 

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // details of one user
        return User::with('role')->find($id);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->only(['name', 'email']));
        return response($user, Response::HTTP_ACCEPTED);
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete one port
        return User::destroy($id);
    }


    
}
