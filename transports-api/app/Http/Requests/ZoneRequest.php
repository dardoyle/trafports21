<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ZoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|unique:zones',
        ];
    }

    /**
     * Messages personnalisés.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nom.required' => 'Vous devez saisir un nom de zone',
            'nom.unique' => 'Cette zone existe déjà',
        ];
    }
}
