<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypePort extends Model
{
    use HasFactory;

    /**
     * Récupère toutes les villes/ tous les ports de ce type
     *
     * @return ports la liste des villes / des ports
     */

    public function ports()
    {
        return $this->hasMany('App\Models\Port');
    }

    protected $fillable = [
        'type',
        
    ];
}
