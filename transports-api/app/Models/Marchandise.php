<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Marchandise extends Model
{
    use HasFactory;

    /**
     * @return nst le code nst de la marchandise
     */
    public function nst(){
        return $this->belongsTo('App\Models\Nst');
    }

    protected $fillable = [
        'nom',
        'intrastat',
        'tarifBateau',
        'tarifCamion',
        'tarifWagon',
    ];
}
