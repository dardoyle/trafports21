<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    use HasFactory;

    /**
     * @return port le port sur lequel l'entreprise est située
     */
    public function port(){
        return $this->belongsTo('App\Models\Port');
    }

    /**
     * @return contrats les contrats de l'entreprise
     */
    public function contrats(){
        return $this->hasMany('App\Models\Contrat');
    }

    protected $fillable = [
        'nom',
        'adresse',
        'tel',
        'mail',
        
    ];
}
