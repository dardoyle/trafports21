<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contrat extends Model
{
    use HasFactory;

    /**
     * @return entreprise l'entreprise à laquelle le contrat appartient
     */
    public function entreprise(){
        return $this->belongsTo('App\Models\Entreprise', 'entreprise_id');
    }

    /**
     * @return port le port relatif au contrat
     */
    public function port(){
        return $this->belongsTo('App\Models\Port', 'ville_id');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User', 'users_has_contrats');
    }



    protected $fillable = [
        'refPolice',
        'refCompta',
        'remarque',
        'foisDeux',
        'dateDeb',
        'dateFin',
    ];
}
