<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Port extends Model
{
    use HasFactory;
    
    /**
     * @return zone la zone à laquelle le port appartient
     */
    public function zone(){
        return $this->belongsTo('App\Models\Zone');
    }

    /**
     * @return pays le pays auquel le port appartient
     */
    public function pays(){
        return $this->belongsTo('App\Models\Pays');
    }

    /**
     * @return typePort le type de port auquel le port appartient
     */
    public function typePort(){
        return $this->belongsTo('App\Models\TypePort');
    }

    /**
     * @return contrats les contrats du port
     */
    public function contrats(){
        return $this->hasMany('App\Models\Contrat');
    }

    



    protected $fillable = [
        'nomFr',
        'nomNdls',
        'nomDeu',
        'nomEn',
        
    ];}
