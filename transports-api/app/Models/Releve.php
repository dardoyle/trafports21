<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Releve extends Model
{
    use HasFactory;

    /**
     * @return user l'utilisateur qui a encodé le relevé
     */
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return marchandise la marchandise transportée
     */
    public function marchandise(){
        return $this->belongsTo('App\Models\Marchandise');
    }

    /**
     * @return transport le moyen de transport utilisé
     */
    public function transport(){
        return $this->belongsTo('App\Models\Transport');
    }

    /**
     * @return statut le statut du relevé (chargement - déchargement - chargement/déchargement)
     */
    public function statut(){
        return $this->belongsTo('App\Models\Statut');
    }

    /**
     * @return contrat le contrat concerné
     */
    public function contrat(){
        return $this->belongsTo('App\Models\Contrat');
    }

    protected $fillable = [
        'date_arrivee',
        'date_depart',
        'quantite',
        'observations',
        
    ];
}
