<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pays extends Model
{
    use HasFactory;

    /**
     * Récupère toutes les villes/ tous les ports qui sont dans ce pays
     *
     * @return villes la liste des villes / des ports
     */
    public function ports()
    {
        return $this->hasMany('App\Models\Port');
    }

    protected $fillable = [
        'nomFr',
        'nomNdls',
        'nomDeu',
        'nomEn',
        
    ];
}
