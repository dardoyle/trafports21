<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nst extends Model
{
    use HasFactory;

    
    
    protected $fillable = [
        'nom',
        'exemples',
        
    ];

    public function marchandises()
    {
        return $this->hasMany('App\Models\Marchandise');
    }
}
