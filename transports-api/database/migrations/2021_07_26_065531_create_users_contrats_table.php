<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersContratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_contrats', function (Blueprint $table) {
            $table->foreignId('user_id');
            $table->foreignId('contrat_id');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('users_contrats', function (Blueprint $table) {
            $table->dropForeign(['user_id', 'contrat_id']);
        });

        
        Schema::dropIfExists('users_contrats');
    }
}
