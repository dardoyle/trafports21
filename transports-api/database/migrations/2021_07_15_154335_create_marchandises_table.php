<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarchandisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marchandises', function (Blueprint $table) {
            $table->id();
            $table->string('nom', 50);
            $table->string('intrastat', 15)->nullable();
            $table->float('tarifBateau', 5, 4)->nullable();
            $table->float('tarifCamion', 5, 4)->nullable();
            $table->float('tarifWagon', 5, 4)->nullable();
            $table->timestamps();
            $table->foreignId('NST_id');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('marchandises', function (Blueprint $table) {
            $table->dropForeign(['NST_id']);
        });

        Schema::dropIfExists('marchandises');
    }
}
