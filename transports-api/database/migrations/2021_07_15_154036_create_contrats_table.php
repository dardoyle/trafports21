<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrats', function (Blueprint $table) {
            $table->id();
            $table->string('refPolice', 20);
            $table->string('refCompta', 20)->nullable();
            $table->text('remarque')->nullable();
            $table->boolean('foisDeux')->default(0);
            $table->dateTime('dateDeb')->nullable();
            $table->dateTime('dateFin')->nullable();
            $table->timestamps();
            $table->foreignId('ville_id');
            $table->foreignId('entreprise_id');

        });

        Schema::enableForeignKeyConstraints();

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrats', function (Blueprint $table) {
            $table->dropForeign(['ville_id', 'entreprise_id']);
        });

        Schema::dropIfExists('contrats');
    }
}
