<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContainerMarchandisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('container_marchandises', function (Blueprint $table) {
            $table->foreignId('container_id');
            $table->foreignId('marchandise_id');
        
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('container_marchandises', function (Blueprint $table) {
            $table->dropForeign(['marchandise_id', 'marchandise_id']);
        });
        Schema::dropIfExists('container_marchandises');

        Schema::dropIfExists('container_marchandises');
    }
}
