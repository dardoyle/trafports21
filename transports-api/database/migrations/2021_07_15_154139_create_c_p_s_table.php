<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCPSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CP', function (Blueprint $table) {
            $table->id();
            $table->string('CP', 10);
            $table->string('ville', 60);
            $table->timestamps();
            $table->foreignId('pays_id');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CP', function (Blueprint $table) {
            $table->dropForeign(['pays_id']);
        });

        Schema::dropIfExists('c_p_s');
    }
}
