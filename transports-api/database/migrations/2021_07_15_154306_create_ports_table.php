<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ports', function (Blueprint $table) {
            $table->id();
            $table->string('nomFr', 50);
            $table->string('nomNdls', 50)->nullable();
            $table->string('nomDeu', 50)->nullable();
            $table->string('nomEn', 50)->nullable();
            $table->timestamps();
            $table->foreignId('type_port_id');
            $table->foreignId('zone_id');
            $table->foreignId('CP_id');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('contrats', function (Blueprint $table) {
            $table->dropForeign(['type_port_id', 'zone_id', 'CP_id']);
        });

        Schema::dropIfExists('ports');
    }
}
