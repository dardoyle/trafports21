<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelevesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('releves', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->date('date_arrivee');
            $table->date('date_depart');
            $table->unsignedInteger('quantite');
            $table->string('observations', 20);
            $table->foreignId('villeProvenance_id');
            $table->foreignId('villeDestination_id');
            $table->foreignId('contrat_id');
            $table->foreignId('transport_id');
            $table->foreignId('statut_id');
            $table->foreignId('marchandise_id');            
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('releves', function (Blueprint $table) {
            $table->dropForeign(['villeProvenance_id', 'villeDestination_id', 'contrat_id', 'transport_id', 'statut_id', 'marchandise_id']);
        });

        Schema::dropIfExists('releves');
    }
}
