<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EntrepriseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // création d'entreprises
        $entreprise = new \App\Models\Entreprise();
        $entreprise->nom = "machin";
        $entreprise->adresse = "rue du Hasard";
        $entreprise->cp_id = "3720";
        $entreprise->tel = "0374 98 71 56";
        $entreprise->mail = "info@machin.be";
        $entreprise->save();

        
    }
}
