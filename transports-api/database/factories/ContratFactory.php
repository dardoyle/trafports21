<?php

namespace Database\Factories;

use App\Models\Contrat;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContratFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contrat::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'refPolice' => $this->faker->word($maxNbChars = 8),
            'refCompta' => $this->faker->word($maxNbChars = 8),
            'remarque' => $this->faker->sentence,
            'foisDeux' => $this->faker->boolean,
            'ville_id'=> $this->faker->numberBetween($min = 3000, $max = 3109),
            'entreprise_id' => $this->faker->numberBetween($min = 9, $max = 28),
        ];
    }
}
