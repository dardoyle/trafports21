<?php

namespace Database\Factories;

use App\Models\Entreprise;
use Illuminate\Database\Eloquent\Factories\Factory;

class EntrepriseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Entreprise::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->company,
            'adresse' => $this->faker->streetAddress,
            'cp_id' => $this->faker->numberBetween($min = 4000, $max = 5600),
            'tel' => $this->faker->e164PhoneNumber,
            'mail'=> $this->faker->email,
            
        ];
    }
}
