import './App.css';
import React from 'react'
import { Layout, Image, Menu, Breadcrumb } from 'antd';
import {
  UploadOutlined, UserOutlined, VideoCameraOutlined, MailOutlined
} from '@ant-design/icons';

const { Header, Footer, Sider, Content } = Layout;
const { SubMenu } = Menu;


class App extends React.Component {
  render(){
    return (
      <div className="App">
        <Layout>
          <Sider
            breakpoint="lg"
            collapsedWidth="0"
            onBreakpoint={broken => {
              console.log(broken);
            }}
            onCollapse={(collapsed, type) => {
              console.log(collapsed, type);
            }}
          >
            <div className="logo">
              <Image
                src="assets/img/logo.png"
              />
            </div>
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['dashboard']}>
            <Menu.Item key='dashboard'>
                  Dashboard
                </Menu.Item>
                <SubMenu
                  key="sub1"
                  title="Navigation One"
                  icon = {<MailOutlined />}
                >
                    <Menu.ItemGroup key='test' title='test'>
                      <Menu.Item key='location1'>Location 1</Menu.Item>
                      <Menu.Item key='location2'>Location 2</Menu.Item>
                    </Menu.ItemGroup>
                </SubMenu>
            </Menu>
          </Sider>
          <Layout>
            <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
            <Content style={{ margin: '24px 16px 0' }}>
              <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
              </Breadcrumb>
              <div className="site-layout-background" style={{ background: '#fff', padding: 24, minHeight: 360 }}>
                content
              </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>AWebStyle ©2021</Footer>
          </Layout>
        </Layout>
      </div>
    );
  }
  }
  

export default App;
